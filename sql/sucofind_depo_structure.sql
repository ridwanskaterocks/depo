-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 14, 2014 at 02:05 PM
-- Server version: 5.5.36-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sucofind_depo`
--

-- --------------------------------------------------------

--
-- Table structure for table `bat_number`
--

CREATE TABLE IF NOT EXISTS `bat_number` (
  `bat_nbr` varchar(5) NOT NULL,
  `active` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `id` varchar(10) NOT NULL,
  `description` varchar(50) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `consignee`
--

CREATE TABLE IF NOT EXISTS `consignee` (
  `id` varchar(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address_line_1` varchar(50) DEFAULT NULL,
  `address_line_2` varchar(50) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `container_uses`
--

CREATE TABLE IF NOT EXISTS `container_uses` (
  `gkey` int(11) NOT NULL AUTO_INCREMENT,
  `eq_nbr` varchar(12) DEFAULT NULL,
  `line_id` varchar(4) DEFAULT NULL,
  `depo_id` varchar(5) DEFAULT NULL,
  `shipping_agents_id` varchar(10) DEFAULT NULL,
  `shipping_agent_name` varchar(50) DEFAULT NULL,
  `in_visit_type_id` varchar(1) NOT NULL DEFAULT 'T',
  `in_time` datetime DEFAULT NULL,
  `out_time` datetime DEFAULT NULL,
  `eq_condition_ingate_check` varchar(1) DEFAULT NULL,
  `eq_condition_yard_check` varchar(1) DEFAULT NULL,
  `eq_condition_outgate_check` varchar(1) DEFAULT NULL,
  `sci_seal_number` varchar(20) DEFAULT NULL,
  `shipper` varchar(20) DEFAULT NULL,
  `creator` varchar(8) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `changer` varchar(8) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `port_of_origin` varchar(5) DEFAULT NULL,
  `consignee` varchar(20) DEFAULT NULL,
  `in_truck_license_nbr` varchar(10) DEFAULT NULL,
  `in_trucking_company_group_id` varchar(10) DEFAULT NULL,
  `in_trucking_company_name` varchar(50) DEFAULT NULL,
  `in_driver_name` varchar(50) DEFAULT NULL,
  `port_of_destination` varchar(5) DEFAULT NULL,
  `depo_name` varchar(50) DEFAULT NULL,
  `out_truck_license_nbr` varchar(10) DEFAULT NULL,
  `out_trucking_company_group_id` varchar(20) DEFAULT NULL,
  `out_trucking_company_name` varchar(50) DEFAULT NULL,
  `out_driver_name` varchar(50) DEFAULT NULL,
  `out_visit_type_id` varchar(1) DEFAULT 'T',
  `bl_nbr` varchar(20) DEFAULT NULL,
  `eq_damage` varchar(1) DEFAULT NULL,
  `status` varchar(1) DEFAULT 'E',
  `category` varchar(2) DEFAULT 'XE',
  `document_verfied` varchar(1) DEFAULT NULL,
  `survey_pos_id` varchar(12) NOT NULL,
  `ctr_position` varchar(12) NOT NULL,
  `trx_id` int(12) NOT NULL,
  PRIMARY KEY (`gkey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(255) NOT NULL AUTO_INCREMENT,
  `tax_id` varchar(25) NOT NULL,
  `name` varchar(75) NOT NULL,
  `addresss` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `postal_code` varchar(12) NOT NULL,
  `created` datetime NOT NULL,
  `creator` varchar(10) NOT NULL,
  `changed` datetime NOT NULL,
  `changer` varchar(10) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Table structure for table `damage_type_codes`
--

CREATE TABLE IF NOT EXISTS `damage_type_codes` (
  `code` varchar(4) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `depo`
--

CREATE TABLE IF NOT EXISTS `depo` (
  `id` varchar(100) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `address_line_1` varchar(50) DEFAULT NULL,
  `address_line_2` varchar(50) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL,
  `country_id` varchar(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `creator` varchar(8) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `changer` varchar(8) DEFAULT NULL,
  `contact_person` varchar(20) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `depo_services`
--

CREATE TABLE IF NOT EXISTS `depo_services` (
  `depo_service_id` varchar(15) NOT NULL,
  `depo_service_name` varchar(50) DEFAULT NULL,
  `billable` varchar(1) DEFAULT NULL,
  `taxable` varchar(1) DEFAULT NULL,
  `active` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`depo_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE IF NOT EXISTS `equipment` (
  `gkey` int(11) NOT NULL AUTO_INCREMENT,
  `eq_prefix` varchar(4) DEFAULT NULL,
  `eq_digit` varchar(7) DEFAULT NULL,
  `eq_check_digit` int(11) DEFAULT NULL,
  `equse_gkey` int(11) NOT NULL,
  `line_owner_id` varchar(12) DEFAULT NULL,
  `tare_weight` int(11) DEFAULT NULL,
  `tare_weight_unit` varchar(2) DEFAULT 'KG',
  `iso_code` varchar(4) DEFAULT NULL,
  `eq_type` varchar(2) DEFAULT NULL,
  `eq_size` varchar(2) DEFAULT NULL,
  `creator` varchar(8) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `changer` varchar(8) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `eq_class` varchar(20) DEFAULT 'CTR',
  `eq_height` varchar(5) DEFAULT NULL,
  `eq_nbr` varchar(12) DEFAULT NULL,
  `line_owner_name` varchar(100) NOT NULL,
  PRIMARY KEY (`gkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=196 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_damages`
--

CREATE TABLE IF NOT EXISTS `equipment_damages` (
  `id_damage` int(255) NOT NULL AUTO_INCREMENT,
  `equse_gkey` int(11) DEFAULT NULL,
  `eq_nbr` varchar(255) NOT NULL,
  `location` varchar(25) NOT NULL,
  `component` varchar(25) NOT NULL,
  `other_damage_code` varchar(5) DEFAULT NULL,
  `other_damage_desc` varchar(50) DEFAULT NULL,
  `location_component` varchar(50) NOT NULL,
  `bent_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Dentet_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Leaking_flag` varchar(1) NOT NULL DEFAULT 'N',
  `PushIn_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Broke_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Hole_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Missing_flag` varchar(1) NOT NULL DEFAULT 'N',
  `PushOut_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Cut_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Loose_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Tom_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Rusty_flag` varchar(1) NOT NULL DEFAULT 'N',
  `survey_location` varchar(15) NOT NULL,
  `InGatephoto` blob,
  `OutGatePhoto` blob,
  `created` datetime NOT NULL,
  `creator` varchar(15) NOT NULL,
  `changed` datetime DEFAULT NULL,
  `changer` varchar(15) DEFAULT NULL,
  `id` varchar(25) NOT NULL,
  `photoinstring` text NOT NULL,
  `filename` varchar(75) NOT NULL,
  `depo_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id_damage`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=433 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_damages_out`
--

CREATE TABLE IF NOT EXISTS `equipment_damages_out` (
  `equse_gkey` int(11) NOT NULL,
  `location` varchar(25) NOT NULL,
  `component` varchar(25) NOT NULL,
  `other_damage_code` varchar(5) DEFAULT NULL,
  `other_damage_desc` varchar(50) DEFAULT NULL,
  `location_component` varchar(50) NOT NULL,
  `bent_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Dentet_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Leaking_flag` varchar(1) NOT NULL DEFAULT 'N',
  `PushIn_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Broke_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Hole_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Missing_flag` varchar(1) NOT NULL DEFAULT 'N',
  `PushOut_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Cut_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Loose_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Tom_flag` varchar(1) NOT NULL DEFAULT 'N',
  `Rusty_flag` varchar(1) NOT NULL DEFAULT 'N',
  `survey_location` varchar(15) NOT NULL,
  `InGatephoto` blob,
  `OutGatePhoto` blob,
  `created` datetime NOT NULL,
  `creator` varchar(15) NOT NULL,
  `changed` datetime DEFAULT NULL,
  `changer` varchar(15) DEFAULT NULL,
  `id` varchar(25) NOT NULL,
  `photoinstring` text NOT NULL,
  `filename` varchar(75) NOT NULL,
  `depo_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_foto`
--

CREATE TABLE IF NOT EXISTS `equipment_foto` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `equse_gkey` varchar(255) DEFAULT NULL,
  `eq_nbr` varchar(255) DEFAULT NULL,
  `depo_id` varchar(255) DEFAULT NULL,
  `leftside` varchar(255) DEFAULT NULL,
  `rightside` varchar(255) DEFAULT NULL,
  `bottomside` varchar(255) DEFAULT NULL,
  `topside` varchar(255) DEFAULT NULL,
  `frontside` varchar(255) DEFAULT NULL,
  `rearside` varchar(255) DEFAULT NULL,
  `inside` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=659 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_history`
--

CREATE TABLE IF NOT EXISTS `equipment_history` (
  `gkey` int(12) NOT NULL AUTO_INCREMENT,
  `equse_gkey` int(12) NOT NULL,
  `eq_nbr` varchar(12) NOT NULL,
  `ctr_position` varchar(25) NOT NULL,
  `creator` varchar(12) NOT NULL,
  `created` datetime NOT NULL,
  `release_status` varchar(1) NOT NULL,
  `status` varchar(5) NOT NULL,
  `depo_id` varchar(15) NOT NULL,
  `survey_pos_id` varchar(15) NOT NULL,
  `category` varchar(5) NOT NULL,
  PRIMARY KEY (`gkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=248 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_services`
--

CREATE TABLE IF NOT EXISTS `equipment_services` (
  `equse_gkey` int(11) DEFAULT NULL,
  `eq_side_area` varchar(20) DEFAULT NULL,
  `survey_condition` varchar(20) DEFAULT NULL,
  `survey_recommendation` varchar(20) DEFAULT NULL,
  `surveyor_ingate_id` varchar(8) DEFAULT NULL,
  `survey_time` datetime DEFAULT NULL,
  `depo_service_area` varchar(20) DEFAULT NULL,
  `depo_serv_id` varchar(10) DEFAULT NULL,
  `depo_serv_name` varchar(20) DEFAULT NULL,
  `condition_after_service` varchar(20) DEFAULT NULL,
  `survey_after_service_time` datetime DEFAULT NULL,
  `surveyor_after_service_id` varchar(8) DEFAULT NULL,
  `surveyor_outgate_id` varchar(8) DEFAULT NULL,
  `survey_outgate_time` datetime DEFAULT NULL,
  `outgate_condition` varchar(20) DEFAULT NULL,
  `outgate_release_status` varchar(20) DEFAULT NULL,
  `serv_gkey` int(12) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`serv_gkey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_temp`
--

CREATE TABLE IF NOT EXISTS `equipment_temp` (
  `eq_nbr` varchar(255) NOT NULL,
  `ready_to_eir_in` varchar(1) NOT NULL,
  `ready_to_eir_out` varchar(1) NOT NULL,
  `survey_pos_id` varchar(255) DEFAULT NULL,
  `depo_id` varchar(100) NOT NULL,
  `iso_code` varchar(4) NOT NULL,
  `tare_weight` int(9) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `line_id` varchar(12) NOT NULL,
  `in_trucking_company_id` varchar(12) NOT NULL,
  `in_trucking_company_name` varchar(50) NOT NULL,
  `in_truck_license_number` varchar(15) NOT NULL,
  `out_trucking_company_id` varchar(12) NOT NULL,
  `out_trucking_company_name` varchar(50) NOT NULL,
  `out_truck_license_number` varchar(15) NOT NULL,
  `equse_gkey` int(11) NOT NULL,
  `doc_valid` varchar(1) NOT NULL,
  `csc_nbr` varchar(25) NOT NULL,
  `max_gross_weight` int(6) NOT NULL,
  `allow_stack` int(6) NOT NULL,
  `racking_test_load` int(6) NOT NULL,
  `date_manufactured` date NOT NULL,
  `ventilation_unit` int(2) NOT NULL,
  PRIMARY KEY (`eq_nbr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equipment_uses`
--

CREATE TABLE IF NOT EXISTS `equipment_uses` (
  `gkey` int(11) NOT NULL AUTO_INCREMENT,
  `eq_nbr` varchar(12) DEFAULT NULL,
  `line_id` varchar(6) DEFAULT NULL,
  `depo_id` varchar(5) DEFAULT NULL,
  `shipping_agents_id` varchar(25) DEFAULT NULL,
  `shipping_agent_name` varchar(75) DEFAULT NULL,
  `in_visit_type_id` varchar(1) NOT NULL DEFAULT 'T',
  `in_time` datetime DEFAULT NULL,
  `out_time` datetime DEFAULT NULL,
  `eq_condition_ingate_check` varchar(1) DEFAULT NULL,
  `eq_condition_yard_check` varchar(1) DEFAULT NULL,
  `eq_condition_outgate_check` varchar(1) DEFAULT NULL,
  `sci_seal_number` varchar(20) DEFAULT NULL,
  `shipper` varchar(20) DEFAULT NULL,
  `creator` varchar(8) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `changer` varchar(8) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `port_of_origin` varchar(5) DEFAULT NULL,
  `consignee` varchar(50) DEFAULT NULL,
  `in_truck_license_nbr` varchar(10) DEFAULT NULL,
  `in_trucking_company_group_id` varchar(10) DEFAULT NULL,
  `in_trucking_company_name` varchar(50) DEFAULT NULL,
  `in_driver_name` varchar(50) DEFAULT NULL,
  `port_of_destination` varchar(5) DEFAULT NULL,
  `depo_name` varchar(50) DEFAULT NULL,
  `out_truck_license_nbr` varchar(10) DEFAULT NULL,
  `out_trucking_company_group_id` varchar(20) DEFAULT NULL,
  `out_trucking_company_name` varchar(50) DEFAULT NULL,
  `out_driver_name` varchar(50) DEFAULT NULL,
  `out_visit_type_id` varchar(1) DEFAULT 'T',
  `out_port_of_origin` varchar(5) NOT NULL,
  `out_port_of_destination` varchar(5) NOT NULL,
  `bl_nbr` varchar(20) DEFAULT NULL,
  `eq_damage` varchar(1) DEFAULT NULL,
  `status` varchar(1) DEFAULT 'E',
  `category` varchar(2) DEFAULT 'XE',
  `document_verfied` varchar(1) DEFAULT NULL,
  `survey_pos_id` varchar(12) NOT NULL,
  `ctr_position` varchar(15) NOT NULL,
  `iso_code` varchar(4) NOT NULL,
  `eq_size` varchar(2) NOT NULL,
  `eq_type` varchar(2) NOT NULL,
  `shipping_line_name` varchar(75) NOT NULL,
  `damage_status` varchar(1) DEFAULT 'N',
  `wash_water_service_flag` varchar(1) DEFAULT 'N',
  `wash_deter_service_flag` varchar(1) DEFAULT 'N',
  `wash_chemi_service_flag` varchar(1) DEFAULT 'N',
  `repair_service_flag` varchar(1) DEFAULT 'N',
  `out_release_status` varchar(1) DEFAULT 'N',
  `out_doc_verified` varchar(1) DEFAULT 'N',
  `shipper_name` varchar(100) DEFAULT NULL,
  `out_damage_status` varchar(1) DEFAULT 'N',
  `ready_to_print_eir` varchar(1) NOT NULL DEFAULT 'N',
  `print_eir_complete` varchar(1) NOT NULL DEFAULT 'N',
  `ready_to_print_eir_out` varchar(1) NOT NULL DEFAULT 'N',
  `print_eir_out_complete` varchar(1) NOT NULL DEFAULT 'N',
  `sci_seal_filename` varchar(75) NOT NULL,
  `seal_plug_check` varchar(1) NOT NULL,
  `trx_id` int(12) NOT NULL,
  `tare_weight_unit` varchar(3) NOT NULL,
  `payment_release` varchar(1) NOT NULL,
  `out_payment_release` varchar(1) NOT NULL,
  `tare_weight` int(9) NOT NULL,
  `trx_ido` int(12) NOT NULL,
  PRIMARY KEY (`gkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=196 ;

--
-- Triggers `equipment_uses`
--
DROP TRIGGER IF EXISTS `trigger_after_insert_equse_insert_or_upadte_eq`;
DELIMITER //
CREATE TRIGGER `trigger_after_insert_equse_insert_or_upadte_eq` AFTER INSERT ON `equipment_uses`
 FOR EACH ROW begin

DECLARE vequsegkey int;
DECLARE veq_nbr, veq_nbr2 varchar(12);
DECLARE error,x int;


set error=0;

select equipment.eq_nbr from equipment where equipment.eq_nbr = new.eq_nbr into veq_nbr2; 

if veq_nbr2 <> "" then
update equipment SET equse_gkey = new.gkey, changer = new.creator, changed = now() WHERE eq_nbr = new.eq_nbr;
else
insert into equipment values (Null,substring(new.eq_nbr,1,4), substring(new.eq_nbr,5,11), substring(new.eq_nbr,11,11), new.gkey, new.line_id, new.tare_weight, 'KG', new.iso_code , new.eq_type, new.eq_size, new.creator, NOW(), NULL, NULL, 'CTR', NULL, new.eq_nbr, new.shipping_line_name);

 end if;

INSERT INTO equipment_history VALUES (null, new.gkey, new.eq_nbr, new.ctr_position, new.creator, now(), 'N', new.status, new.depo_id, new.survey_pos_id, new.category);
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `invoice_nbr` varchar(255) NOT NULL,
  `trx_id` int(255) NOT NULL,
  `iso_code` varchar(100) DEFAULT NULL,
  `qty_of_20` int(10) DEFAULT NULL,
  `qty_of_40` int(10) DEFAULT NULL,
  `total` int(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `creator` varchar(255) NOT NULL,
  PRIMARY KEY (`invoice_nbr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `iso_code`
--

CREATE TABLE IF NOT EXISTS `iso_code` (
  `iso_code_id` varchar(4) NOT NULL,
  `size` varchar(2) DEFAULT NULL,
  `type` varchar(2) DEFAULT NULL,
  `height` varchar(5) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `TareWeight` int(11) NOT NULL,
  PRIMARY KEY (`iso_code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `authorized` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1652 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_input`
--

CREATE TABLE IF NOT EXISTS `log_input` (
  `id_log` int(255) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `data` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log_users`
--

CREATE TABLE IF NOT EXISTS `log_users` (
  `id_log` int(255) NOT NULL AUTO_INCREMENT,
  `id_user` int(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=784 ;

-- --------------------------------------------------------

--
-- Table structure for table `option`
--

CREATE TABLE IF NOT EXISTS `option` (
  `id_option` int(255) NOT NULL AUTO_INCREMENT,
  `option_name` text NOT NULL,
  `option_value` text NOT NULL,
  PRIMARY KEY (`id_option`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `portcodes`
--

CREATE TABLE IF NOT EXISTS `portcodes` (
  `id` varchar(10) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(25) NOT NULL,
  `country` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `print_eir`
--

CREATE TABLE IF NOT EXISTS `print_eir` (
  `invoice_nbr` int(255) NOT NULL,
  `trx_id` int(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator` varchar(255) NOT NULL,
  `complete` varchar(100) NOT NULL,
  `equse_gkey` int(255) NOT NULL,
  `eq_nbr` varchar(25) NOT NULL,
  PRIMARY KEY (`invoice_nbr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `repair_codes`
--

CREATE TABLE IF NOT EXISTS `repair_codes` (
  `code` varchar(4) NOT NULL,
  `description` varchar(100) NOT NULL,
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles_menu`
--

CREATE TABLE IF NOT EXISTS `roles_menu` (
  `role_Id` varchar(12) NOT NULL,
  `Ingate_menu` varchar(1) NOT NULL,
  `service_area_menu` varchar(1) NOT NULL,
  `out_gate_menu` varchar(1) NOT NULL,
  `statistic_menu` varchar(1) NOT NULL,
  `admin_menu` varchar(1) NOT NULL,
  `reference_menu` varchar(1) NOT NULL,
  `preadvise_menu` varchar(1) NOT NULL,
  `delivery_menu` varchar(1) NOT NULL,
  `invoice_menu` varchar(1) NOT NULL,
  `print_eir_menu` varchar(1) NOT NULL,
  `repo_menu` varchar(1) NOT NULL,
  UNIQUE KEY `role_Id` (`role_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_recommendations`
--

CREATE TABLE IF NOT EXISTS `service_recommendations` (
  `recommend_id` varchar(10) NOT NULL DEFAULT '',
  `recommend_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`recommend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shippers`
--

CREATE TABLE IF NOT EXISTS `shippers` (
  `id` varchar(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address_line_1` varchar(50) DEFAULT NULL,
  `address_line_2` varchar(50) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_agents`
--

CREATE TABLE IF NOT EXISTS `shipping_agents` (
  `id` varchar(15) NOT NULL DEFAULT '',
  `name` varchar(50) DEFAULT NULL,
  `address line 1` varchar(50) DEFAULT NULL,
  `address line 2` varchar(50) DEFAULT NULL,
  `contact person` varchar(50) DEFAULT NULL,
  `telepon` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `creator` varchar(8) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `changer` varchar(8) DEFAULT NULL,
  `line_id` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_lines`
--

CREATE TABLE IF NOT EXISTS `shipping_lines` (
  `Id` varchar(5) NOT NULL,
  `name` varchar(30) NOT NULL,
  `remarks` varchar(50) NOT NULL,
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sides`
--

CREATE TABLE IF NOT EXISTS `sides` (
  `name` varchar(20) NOT NULL,
  `description` varchar(75) NOT NULL,
  `component` varchar(15) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tarrif`
--

CREATE TABLE IF NOT EXISTS `tarrif` (
  `id` varchar(5) NOT NULL,
  `eq_size` varchar(5) NOT NULL,
  `eq_type` varchar(5) NOT NULL,
  `tarrif_rate` int(11) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `Status` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `trx_id` int(12) NOT NULL AUTO_INCREMENT,
  `customer_tax_id` varchar(25) NOT NULL,
  `customer_name` varchar(75) NOT NULL,
  `vessel_id` varchar(25) NOT NULL,
  `vessel_voyage_id` varchar(6) NOT NULL,
  `vessel_name` varchar(50) NOT NULL,
  `doc_number` varchar(25) NOT NULL,
  `trx_type_id` varchar(5) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `trx_type_name` varchar(25) NOT NULL,
  `created` datetime NOT NULL,
  `creator` varchar(10) NOT NULL,
  `changed` datetime NOT NULL,
  `changer` varchar(10) NOT NULL,
  `start_timestamp` datetime NOT NULL,
  `end_timestamp` datetime NOT NULL,
  `bat_nbr` varchar(5) NOT NULL,
  `shipping_agent_id` varchar(15) NOT NULL,
  `shipping_agent_name` varchar(25) NOT NULL,
  `trucking_company_id` varchar(100) NOT NULL,
  `trucking_company_name` varchar(50) NOT NULL,
  `depo_release` varchar(1) NOT NULL,
  `payment_release` varchar(1) NOT NULL,
  `depo_id` varchar(100) NOT NULL,
  `truck_license_nbr` varchar(15) NOT NULL,
  PRIMARY KEY (`trx_id`),
  UNIQUE KEY `trx_id` (`trx_id`),
  UNIQUE KEY `trx_id_2` (`trx_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE IF NOT EXISTS `transaction_type` (
  `id` varchar(5) NOT NULL,
  `name` varchar(25) NOT NULL,
  `billable` varchar(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trucking_company`
--

CREATE TABLE IF NOT EXISTS `trucking_company` (
  `id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(23) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `device_id` varchar(12) DEFAULT NULL,
  `depo_id` varchar(25) DEFAULT NULL,
  `role_id` varchar(12) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `unique_id` (`unique_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_Id` varchar(12) NOT NULL,
  `role_Id` varchar(12) NOT NULL,
  `gkey` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`gkey`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `vessels`
--

CREATE TABLE IF NOT EXISTS `vessels` (
  `id` varchar(15) NOT NULL,
  `name` varchar(50) NOT NULL,
  `callsign` varchar(10) NOT NULL,
  `line_owner` varchar(10) NOT NULL,
  `country` varchar(15) NOT NULL,
  `year` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vessel_visits`
--

CREATE TABLE IF NOT EXISTS `vessel_visits` (
  `voy_id` varchar(10) NOT NULL,
  `ship_id` varchar(15) NOT NULL,
  `ship_name` varchar(50) NOT NULL,
  PRIMARY KEY (`voy_id`),
  UNIQUE KEY `voy_id` (`voy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
