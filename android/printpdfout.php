<?php
$equse_gkey=$_GET["equse_gkey"];
//PDF USING MULTIPLE PAGES

define('FPDF_FONTPATH','fpdf17/font/'); 
require('fpdf17/fpdf.php');

//Connect to your database
include("connection.php");

class PDF extends FPDF
{
	
var $tablewidths; 
var $headerset; 
var $footerset; 


	//Page header
	function Header()
	{
		//Logo
		$this->Image('logo/logo-ubl.jpg',10,8);
		//Arial bold 15
		$this->SetFont('Arial','B',15);
		//pindah ke posisi ke tengah untuk membuat judul
		$this->Cell(80);
		//judul
		$this->Cell(30,10,'EQUIPMENT INTERCHANGE RECEIPT (EIR)',0,0,'C');
		//pindah baris
		$this->Ln(20);
		//buat garis horisontal
		$this->Line(10,25,250,25);
	}
	
	//Page Content
	function Content()
	{
		$this->SetFont('Times','',12);
		for($i=1; $i<=40; $i++)
			$this->Cell(0,10,'report '.$i,0,1);
	}

	//Page footer
	function Footer()
	{
		//atur posisi 1.5 cm dari bawah
		$this->SetY(-15);
		//buat garis horizontal
		$this->Line(10,$this->GetY(),200,$this->GetY());
		//Arial italic 9
		$this->SetFont('Arial','I',9);
		//nomor halaman
		$this->Cell(-15,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}

//Create new pdf file
$pdf=new FPDF();
//$pdf->AliasNbPages();
//Disable automatic page break
$pdf->SetAutoPageBreak(false);

//Add first page
$pdf->_beginpage('L','A4');
		$pdf->Image('logo/asdekilogo.jpg',10,8);
		$pdf->Image('logo/scilogo.jpg',200,8);
		//Arial bold 15
		$pdf->SetFont('Arial','B',15);
		//pindah ke posisi ke tengah untuk membuat judul
		$pdf->Cell(80);
		//judul
		$pdf->Cell(50,10,'EQUIPMENT INTERCHANGE RECEIPT (EIR)',0,0,'C');
		$pdf->Ln(10);
		$pdf->SetFont('Arial','B',15);
		//pindah ke posisi ke tengah untuk membuat sub judul
		$pdf->Cell(80);
		$pdf->Cell(50,10,'Outgoing Container',0,0,'C');
		//pindah baris
		
		$pdf->Ln(30);
		//buat garis horisontal
		$pdf->Line(10,35,225,35);
		
		//Select the container & customer data as a Tittle

$result1=mysql_query("select gkey, eq_nbr, iso_code, eq_size, eq_type, line_id, shipping_agent_name, depo_id, in_time, in_trucking_company_group_id,in_trucking_company_name,in_truck_license_nbr,in_time, sci_seal_number, out_trucking_company_group_id,out_trucking_company_name,out_truck_license_nbr, out_time , shipper, shipper_name, creator,changer from equipment_uses where gkey = '".$equse_gkey."' ");
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0);
$pdf->SetFont('');
$pdf->SetX(15);

while($row1 = mysql_fetch_array($result1))
{
	$shipper_name= $row1['shipper_name'];
	$gkey= $row1['gkey'];
	$eq_nbr= $row1['eq_nbr'];
	$iso_code= $row1['iso_code'];
	$eq_size= $row1['eq_size'];
	$eq_type= $row1['eq_type'];
	$depo_id= $row1['depo_id'];
	$out_trucking_company_group_id= $row1['out_trucking_company_group_id'];
	$out_trucking_company_name= $row1['out_trucking_company_name'];
	$out_truck_license_nbr = $row1['out_truck_license_nbr'];
	$out_time  = $row1['out_time'];
	$sci_seal_number= $row1['sci_seal_number'];
	
	$pdf->SetX(15);
	$pdf->SetFillColor(255,0,0);
	$pdf->SetTextColor(255);
	$pdf->SetDrawColor(128,0,0);
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(225,6,'Info Depo',1,0,'C',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->SetFillColor(224,235,255);
	$pdf->SetTextColor(0);
	$pdf->SetFont('Arial','',14);
	$pdf->Cell(50,6,'No Transaksi',1,0,'L',1);
	$pdf->Cell(175,6,$gkey,1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->Cell(50,6,'Depo',1,0,'L',1);
	$pdf->Cell(175,6,$depo_id,1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->Cell(50,6,'Alamat Depo',1,0,'L',1);
	$pdf->Cell(175,6,'',1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->Cell(50,6,'Manajer Depo',1,0,'L',1);
	$pdf->Cell(175,6,'',1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->SetFillColor(255,0,0);
	$pdf->SetTextColor(255);
	$pdf->SetDrawColor(128,0,0);
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(225,6,'Info Petikemas',1,0,'C',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->SetFillColor(224,235,255);
	$pdf->SetTextColor(0);
	$pdf->SetFont('Arial','',14);
	$pdf->Cell(100,6,'Eksportir',1,0,'L',1);
	$pdf->Cell(125,6,$shipper_name,1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->Cell(50,6,'Container Number',1,0,'L',1);
	$pdf->Cell(50,6,$eq_nbr,1,0,'L',1);
	$pdf->Cell(25,6,'Iso Code',1,0,'L',1);
	$pdf->Cell(25,6,$iso_code,1,0,'L',1);
	$pdf->Cell(25,6,$eq_size,1,0,'L',1);
	$pdf->Cell(25,6,$eq_type,1,0,'L',1);
	$pdf->Cell(25,6,'96',1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->SetFillColor(255,0,0);
	$pdf->SetTextColor(255);
	$pdf->SetDrawColor(128,0,0);
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(225,6,'Info Trucking',1,0,'C',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->SetFillColor(224,235,255);
	$pdf->SetTextColor(0);
	$pdf->SetFont('Arial','',14);
	$pdf->Cell(50,6,'Trucking Company',1,0,'L',1);
	$pdf->Cell(50,6,$out_trucking_company_group_id,1,0,'L',1);
	$pdf->Cell(125,6,$out_trucking_company_name,1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->Cell(50,6,'No Polisi Truk',1,0,'L',1);
	$pdf->Cell(50,6,$out_truck_license_nbr,1,0,'L',1);
	$pdf->Cell(50,6,'Tgl/Jam Keluar',1,0,'L',1);
	$pdf->Cell(75,6,$out_time,1,0,'L',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->SetFillColor(255,0,0);
	$pdf->SetTextColor(255);
	$pdf->SetDrawColor(128,0,0);
	$pdf->SetFont('Arial','B',16);
	$pdf->Cell(225,6,'Info SCI SEAL',1,0,'C',1);
	$pdf->Ln();
	$pdf->SetX(15);
	$pdf->SetFillColor(224,235,255);
	$pdf->SetTextColor(0);
	$pdf->SetFont('Arial','',14);
	$pdf->Cell(50,20,'SCI SEAL #',1,0,'L',1);
	$pdf->Cell(50,20,$sci_seal_number,1,0,'L',1);
	$pdf->Cell(50,20,'Bar Code #',1,0,'L',1);
	$pdf->Cell(75,20,'',1,0,'L',1);
	
	
}
//atur posisi 1.5 cm dari bawah
		$pdf->SetY(-15);
		//buat garis horizontal
		$pdf->Line(10,$pdf->GetY(),275,$pdf->GetY());
		//Arial italic 9
		$pdf->SetFont('Arial','I',9);
		//nomor halaman
		$pdf->Cell(0,10,'Dicetak secara sistem oleh '.$depo_id.'',0,0,'C');
		$pdf->Cell(-15,10,'Halaman '.$pdf->PageNo().'',0,0,'R');

$pdf->Header();
$pdf->AddPage('L');

		
//set initial y axis position per page
$y_axis_initial = 25;
$y_axis=25;

//initialize counter
$i = 0;

//Set maximum rows per page
$max = 1;

//Set Row Height
$row_height = 6;

$pdf->SetFillColor(255,0,0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128,0,0);

//print column titles

$pdf->SetFont('Arial','B',14);
$pdf->SetY($y_axis_initial);
$pdf->SetX(15);
$pdf->Cell(40,6,'COMPONENT',1,0,'L',1);
$pdf->Cell(40,6,'LOCATION',1,0,'L',1);
$pdf->Cell(40,6,'SURVEY AREA',1,0,'L',1);
$pdf->Cell(10,6,'BE',1,0,'L',1);
$pdf->Cell(10,6,'DE',1,0,'L',1);
$pdf->Cell(10,6,'LE',1,0,'L',1);
$pdf->Cell(10,6,'PI',1,0,'L',1);
$pdf->Cell(10,6,'BR',1,0,'L',1);
$pdf->Cell(10,6,'HO',1,0,'L',1);
$pdf->Cell(10,6,'MI',1,0,'L',1);
$pdf->Cell(10,6,'PO',1,0,'L',1);
$pdf->Cell(10,6,'CU',1,0,'L',1);
$pdf->Cell(10,6,'LO',1,0,'L',1);
$pdf->Cell(10,6,'TO',1,0,'L',1);
$pdf->Cell(10,6,'RU',1,0,'L',1);


$y_axis = $y_axis + $row_height;

//Select the damage component you want to show in your PDF file
$result=mysql_query("select component, location,survey_location,if(bent_flag='Y','Y','-') as bent_flag, if(Dentet_flag='Y','Y','-') as Dentet_flag,if(Leaking_flag='Y','Y','-') as Leaking_flag,if(PushIn_flag='Y','Y','-') as PushIn_flag,if(Broke_flag='Y','Y','-') as Broke_flag,if(Hole_flag='Y','Y','-') as Hole_flag,if(Missing_flag='Y','Y','-') as Missing_flag,if(PushOut_flag='Y','Y','-') as PushOut_flag, if(Cut_flag='Y','Y','-') as Cut_flag, if(Loose_flag='Y','Y','-') as Loose_flag,if(Tom_flag='Y','Y','-') as Tom_flag, if(Rusty_flag='Y','Y','-') as Rusty_flag, if(filename='','N',filename) as filename from equipment_damages_out where equse_gkey = '".$equse_gkey."' ORDER BY survey_location");

$fill=false;
$pdf->SetTextColor(0);
$pdf->SetFont('Arial','',12);
while($row = mysql_fetch_array($result))
{
	//If the current row is the last one, create new page and print column title
	if ($i == $max)
	{
		// print footer for previous page
		
		$pdf->SetY(-15);
		//buat garis horizontal
		$pdf->Line(10,$pdf->GetY(),275,$pdf->GetY());
		//Arial italic 9
		$pdf->SetFont('Arial','I',9);
		//nomor halaman
		$pdf->Cell(0,10,'Dicetak secara sistem oleh '.$depo_id.'',0,0,'C');
		$pdf->Cell(-15,10,'Halaman '.$pdf->PageNo().'',0,0,'R');
		
		$pdf->AddPage('L');
		
		//Set Row Height
		$row_height = 6;

		$pdf->SetFillColor(255,0,0);
		$pdf->SetTextColor(255);
		$pdf->SetDrawColor(128,0,0);


		$pdf->SetFont('Arial','B',14);
		$pdf->SetY($y_axis_initial);
		$pdf->SetX(15);


		//print column titles for the current page
	
		
		$pdf->Cell(40,6,'COMPONENT',1,0,'L',1);
		$pdf->Cell(40,6,'LOCATION',1,0,'L',1);
		$pdf->Cell(40,6,'SURVEY AREA',1,0,'L',1);
		$pdf->Cell(10,6,'BE',1,0,'L',1);
		$pdf->Cell(10,6,'DE',1,0,'L',1);
		$pdf->Cell(10,6,'LE',1,0,'L',1);
		$pdf->Cell(10,6,'PI',1,0,'L',1);
		$pdf->Cell(10,6,'BR',1,0,'L',1);
		$pdf->Cell(10,6,'HO',1,0,'L',1);
		$pdf->Cell(10,6,'MI',1,0,'L',1);
		$pdf->Cell(10,6,'PO',1,0,'L',1);
		$pdf->Cell(10,6,'CU',1,0,'L',1);
		$pdf->Cell(10,6,'LO',1,0,'L',1);
		$pdf->Cell(10,6,'TO',1,0,'L',1);
		$pdf->Cell(10,6,'RU',1,0,'L',1);
		
		//Go to next row
		$y_axis = $y_axis_initial + $row_height;
		
		//Set $i variable to 0 (first row)
		$i = 0;
		//$y_axis=25;
	}
	
	$component= $row['component'];
	$location= $row['location'];
	$survey_location= $row['survey_location'];
	$bent_flag= $row['bent_flag'];
	$Dentet_flag= $row['Dentet_flag'];
	$Leaking_flag= $row['Leaking_flag'];
	$PushIn_flag= $row['PushIn_flag'];
	$Broke_flag= $row['Broke_flag'];
	$Hole_flag= $row['Hole_flag'];
	$Missing_flag= $row['Missing_flag'];
	$PushOut_flag= $row['PushOut_flag'];
	$Cut_flag= $row['Cut_flag'];
	$Loose_flag= $row['Loose_flag'];
	$Tom_flag= $row['Tom_flag'];
	$Rusty_flag= $row['Rusty_flag'];
	$filename=$row['filename'];
	if ($fill==false)
	{
	$pdf->SetFillColor(224,235,255);
	$fill=false;
	}
	else
	{
	$pdf->SetFillColor(255,235,255);
	$fill=true;
	}

	$pdf->SetTextColor(0);
	$pdf->SetFont('Arial','',12);
	
	$pdf->SetY($y_axis);
	$pdf->SetX(15);
	$pdf->Cell(40,6,$component,1,0,'L',1);
	$pdf->Cell(40,6,$location,1,0,'L',1);
	$pdf->Cell(40,6,$survey_location,1,0,'L',1);
        $pdf->Cell(10,6,$bent_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Dentet_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Leaking_flag,1,0,'L',1);
	$pdf->Cell(10,6,$PushIn_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Broke_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Hole_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Missing_flag,1,0,'L',1);
	$pdf->Cell(10,6,$PushOut_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Cut_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Loose_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Tom_flag,1,0,'L',1);
	$pdf->Cell(10,6,$Rusty_flag,1,0,'L',1);
	if ($filename !='N')
	{
		$pdf->Ln(20);
		//Arial bold 15
		$pdf->SetFont('Arial','B',14);
		//pindah ke posisi ke tengah untuk membuat judul
		$pdf->Cell(80);
		//judul
		$pdf->Cell(50,10,'DAMAGE PHOTO',0,0,'C');
		$pdf->Ln();
		$pdf->Cell(80);
		$pdf->Cell(50,10,$filename,0,0,'C');
		//pindah baris
		
		$pdf->Ln(30);
		//buat garis horisontal
		$pdf->Line(20,50,250,50);
		$pdf->Image('files/images/damage/'.$filename.'',70,80);	
	}
	else
	{
	$pdf->Ln(20);
		//Arial bold 15
		$pdf->SetFont('Arial','B',14);
		//pindah ke posisi ke tengah untuk membuat judul
		$pdf->Cell(80);
		//judul
		$pdf->Cell(50,10,'DAMAGE PHOTO',0,0,'C');
		$pdf->Ln();
		$pdf->Cell(80);
		$pdf->Cell(50,10,'No Photo Captured',0,0,'C');	
		
	}
	
		
	//Go to next row
	$y_axis = $y_axis + $row_height;
	$i = $i + 1;
	$fill = !$fill;
}

//mysql_close($link);

//Send file
$pdf->Output();
?>
