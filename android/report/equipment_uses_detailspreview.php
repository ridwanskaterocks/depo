<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

include("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

include("include/equipment_uses_variables.php");

$mode=postvalue("mode");

if(!@$_SESSION["UserID"])
{ 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
{
	return;
}

include('include/xtempl.php');
$xt = new Xtempl();

$layout = new TLayout("detailspreview","BoldOrange","MobileOrange");
$layout->blocks["bare"] = array();
$layout->containers["0"] = array();

$layout->containers["0"][] = array("name"=>"detailspreviewheader","block"=>"","substyle"=>1);


$layout->skins["0"] = "empty";
$layout->blocks["bare"][] = "0";
$layout->containers["0"] = array();

$layout->containers["0"][] = array("name"=>"detailspreviewdetailsfount","block"=>"","substyle"=>1);


$layout->containers["0"][] = array("name"=>"detailspreviewdispfirst","block"=>"display_first","substyle"=>1);


$layout->skins["0"] = "empty";
$layout->blocks["bare"][] = "0";
$layout->containers["detailspreviewgrid"] = array();

$layout->containers["detailspreviewgrid"][] = array("name"=>"detailspreviewfields","block"=>"details_data","substyle"=>1);


$layout->skins["detailspreviewgrid"] = "grid";
$layout->blocks["bare"][] = "detailspreviewgrid";$page_layouts["equipment_uses_detailspreview"] = $layout;


$recordsCounter = 0;

//	process masterkey value
$mastertable=postvalue("mastertable");
if($mastertable!="")
{
	$_SESSION[$strTableName."_mastertable"]=$mastertable;
//	copy keys to session
	$i=1;
	while(isset($_REQUEST["masterkey".$i]))
	{
		$_SESSION[$strTableName."_masterkey".$i]=$_REQUEST["masterkey".$i];
		$i++;
	}
	if(isset($_SESSION[$strTableName."_masterkey".$i]))
		unset($_SESSION[$strTableName."_masterkey".$i]);
}
else
	$mastertable=$_SESSION[$strTableName."_mastertable"];

//$strSQL = $gstrSQL;

if($mastertable=="transaction")
{
	$where ="";
		$where.= GetFullFieldName("trx_id")."=".make_db_value("trx_id",$_SESSION[$strTableName."_masterkey1"]);
}


$str = SecuritySQL("Search");
if(strlen($str))
	$where.=" and ".$str;
$strSQL = gSQLWhere($where);

$strSQL.=" ".$gstrOrderBy;

$rowcount=gSQLRowCount($where);

$xt->assign("row_count",$rowcount);
if ( $rowcount ) {
	$xt->assign("details_data",true);
	$rs=db_query($strSQL,$conn);

	$display_count=10;
	if($mode=="inline")
		$display_count*=2;
	if($rowcount>$display_count+2)
	{
		$xt->assign("display_first",true);
		$xt->assign("display_count",$display_count);
	}
	else
		$display_count = $rowcount;

	$rowinfo=array();
		
	while (($data = db_fetch_array($rs)) && $recordsCounter<$display_count) {
		$recordsCounter++;
		$row=array();
		$keylink="";
		$keylink.="&key1=".htmlspecialchars(rawurlencode(@$data["gkey"]));

	
	//	gkey - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"gkey", ""),"field=gkey".$keylink,"",MODE_PRINT);
			$row["gkey_value"]=$value;
	//	eq_nbr - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"eq_nbr", ""),"field=eq%5Fnbr".$keylink,"",MODE_PRINT);
			$row["eq_nbr_value"]=$value;
	//	line_id - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"line_id", ""),"field=line%5Fid".$keylink,"",MODE_PRINT);
			$row["line_id_value"]=$value;
	//	depo_id - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"depo_id", ""),"field=depo%5Fid".$keylink,"",MODE_PRINT);
			$row["depo_id_value"]=$value;
	//	shipping_agents_id - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"shipping_agents_id", ""),"field=shipping%5Fagents%5Fid".$keylink,"",MODE_PRINT);
			$row["shipping_agents_id_value"]=$value;
	//	shipping_agent_name - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"shipping_agent_name", ""),"field=shipping%5Fagent%5Fname".$keylink,"",MODE_PRINT);
			$row["shipping_agent_name_value"]=$value;
	//	creator - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"creator", ""),"field=creator".$keylink,"",MODE_PRINT);
			$row["creator_value"]=$value;
	//	created - Short Date
		    $value="";
				$value = ProcessLargeText(GetData($data,"created", "Short Date"),"field=created".$keylink,"",MODE_PRINT);
			$row["created_value"]=$value;
	//	port_of_origin - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"port_of_origin", ""),"field=port%5Fof%5Forigin".$keylink,"",MODE_PRINT);
			$row["port_of_origin_value"]=$value;
	//	consignee - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"consignee", ""),"field=consignee".$keylink,"",MODE_PRINT);
			$row["consignee_value"]=$value;
	//	port_of_destination - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"port_of_destination", ""),"field=port%5Fof%5Fdestination".$keylink,"",MODE_PRINT);
			$row["port_of_destination_value"]=$value;
	//	bl_nbr - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"bl_nbr", ""),"field=bl%5Fnbr".$keylink,"",MODE_PRINT);
			$row["bl_nbr_value"]=$value;
	//	category - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"category", ""),"field=category".$keylink,"",MODE_PRINT);
			$row["category_value"]=$value;
	//	document_verfied - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"document_verfied", ""),"field=document%5Fverfied".$keylink,"",MODE_PRINT);
			$row["document_verfied_value"]=$value;
	//	survey_pos_id - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"survey_pos_id", ""),"field=survey%5Fpos%5Fid".$keylink,"",MODE_PRINT);
			$row["survey_pos_id_value"]=$value;
	//	ctr_position - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"ctr_position", ""),"field=ctr%5Fposition".$keylink,"",MODE_PRINT);
			$row["ctr_position_value"]=$value;
	//	iso_code - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"iso_code", ""),"field=iso%5Fcode".$keylink,"",MODE_PRINT);
			$row["iso_code_value"]=$value;
	//	eq_size - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"eq_size", ""),"field=eq%5Fsize".$keylink,"",MODE_PRINT);
			$row["eq_size_value"]=$value;
	//	eq_type - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"eq_type", ""),"field=eq%5Ftype".$keylink,"",MODE_PRINT);
			$row["eq_type_value"]=$value;
	//	shipping_line_name - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"shipping_line_name", ""),"field=shipping%5Fline%5Fname".$keylink,"",MODE_PRINT);
			$row["shipping_line_name_value"]=$value;
	//	tare_weight_unit - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"tare_weight_unit", ""),"field=tare%5Fweight%5Funit".$keylink,"",MODE_PRINT);
			$row["tare_weight_unit_value"]=$value;
	//	payment_release - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"payment_release", ""),"field=payment%5Frelease".$keylink,"",MODE_PRINT);
			$row["payment_release_value"]=$value;
	//	trx_id - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"trx_id", ""),"field=trx%5Fid".$keylink,"",MODE_PRINT);
			$row["trx_id_value"]=$value;
	$rowinfo[]=$row;
	}
	$xt->assign_loopsection("details_row",$rowinfo);
} else {
}
$xt->display("equipment_uses_detailspreview.htm");
if($mode!="inline"){
	echo "counterSeparator".postvalue("counter");
	$layout = GetPageLayout(GoodFieldName($strTableName), 'detailspreview');
	if($layout)
	{
		$rtl = $xt->getReadingOrder() == 'RTL' ? 'RTL' : '';
		echo "counterSeparator"."styles/".$layout->style."/style".$rtl
			."counterSeparator"."pagestyles/".$layout->name.$rtl
			."counterSeparator".$layout->style." page-".$layout->name;
	}	
}	
?>