<?php
$tdatatransaction=array();
	$tdatatransaction[".NumberOfChars"]=80; 
	$tdatatransaction[".ShortName"]="transaction";
	$tdatatransaction[".OwnerID"]="";
	$tdatatransaction[".OriginalTable"]="transaction";


	
//	field labels
$fieldLabelstransaction = array();
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelstransaction["English"]=array();
	$fieldToolTipstransaction["English"]=array();
	$fieldLabelstransaction["English"]["trx_id"] = "Trx Id";
	$fieldToolTipstransaction["English"]["trx_id"] = "";
	$fieldLabelstransaction["English"]["depo_id"] = "Depo Id";
	$fieldToolTipstransaction["English"]["depo_id"] = "";
	$fieldLabelstransaction["English"]["trx_type_id"] = "Trx Type Id";
	$fieldToolTipstransaction["English"]["trx_type_id"] = "";
	$fieldLabelstransaction["English"]["doc_number"] = "Doc Number";
	$fieldToolTipstransaction["English"]["doc_number"] = "";
	$fieldLabelstransaction["English"]["vessel_name"] = "Vessel Name";
	$fieldToolTipstransaction["English"]["vessel_name"] = "";
	$fieldLabelstransaction["English"]["vessel_voyage_id"] = "Vessel Voyage Id";
	$fieldToolTipstransaction["English"]["vessel_voyage_id"] = "";
	$fieldLabelstransaction["English"]["vessel_id"] = "Vessel Id";
	$fieldToolTipstransaction["English"]["vessel_id"] = "";
	$fieldLabelstransaction["English"]["customer_name"] = "Customer Name";
	$fieldToolTipstransaction["English"]["customer_name"] = "";
	$fieldLabelstransaction["English"]["shipping_agent_id"] = "Shipping Agent Id";
	$fieldToolTipstransaction["English"]["shipping_agent_id"] = "";
	$fieldLabelstransaction["English"]["shipping_agent_name"] = "Shipping Agent Name";
	$fieldToolTipstransaction["English"]["shipping_agent_name"] = "";
	if (count($fieldToolTipstransaction["English"])){
		$tdatatransaction[".isUseToolTips"]=true;
	}
}


	
	$tdatatransaction[".NCSearch"]=true;

	

$tdatatransaction[".shortTableName"] = "transaction";
$tdatatransaction[".nSecOptions"] = 0;
$tdatatransaction[".recsPerRowList"] = 1;	
$tdatatransaction[".tableGroupBy"] = "0";
$tdatatransaction[".mainTableOwnerID"] = "";
$tdatatransaction[".moveNext"] = 1;




$tdatatransaction[".showAddInPopup"] = false;

$tdatatransaction[".showEditInPopup"] = false;

$tdatatransaction[".showViewInPopup"] = false;


$tdatatransaction[".fieldsForRegister"] = array();

$tdatatransaction[".listAjax"] = false;

	$tdatatransaction[".audit"] = false;

	$tdatatransaction[".locking"] = false;
	
$tdatatransaction[".listIcons"] = true;

$tdatatransaction[".exportTo"] = true;

$tdatatransaction[".printFriendly"] = true;


$tdatatransaction[".showSimpleSearchOptions"] = false;

$tdatatransaction[".showSearchPanel"] = true;


$tdatatransaction[".isUseAjaxSuggest"] = true;

$tdatatransaction[".rowHighlite"] = true;


// button handlers file names

$tdatatransaction[".addPageEvents"] = false;

$tdatatransaction[".arrKeyFields"][] = "trx_id";

// use datepicker for search panel
$tdatatransaction[".isUseCalendarForSearch"] = false;

// use timepicker for search panel
$tdatatransaction[".isUseTimeForSearch"] = false;

$tdatatransaction[".isUseiBox"] = false;


	

	

$tdatatransaction[".useDetailsPreview"] = true;	


$tdatatransaction[".isUseInlineJs"] = $tdatatransaction[".isUseInlineAdd"] || $tdatatransaction[".isUseInlineEdit"];

$tdatatransaction[".allSearchFields"] = array();

$tdatatransaction[".globSearchFields"][] = "trx_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_id", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "trx_id";	
}
$tdatatransaction[".globSearchFields"][] = "depo_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("depo_id", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "depo_id";	
}
$tdatatransaction[".globSearchFields"][] = "trx_type_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_type_id", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "trx_type_id";	
}
$tdatatransaction[".globSearchFields"][] = "doc_number";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("doc_number", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "doc_number";	
}
$tdatatransaction[".globSearchFields"][] = "vessel_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("vessel_name", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "vessel_name";	
}
$tdatatransaction[".globSearchFields"][] = "vessel_voyage_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("vessel_voyage_id", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "vessel_voyage_id";	
}
$tdatatransaction[".globSearchFields"][] = "vessel_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("vessel_id", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "vessel_id";	
}
$tdatatransaction[".globSearchFields"][] = "customer_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("customer_name", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "customer_name";	
}
$tdatatransaction[".globSearchFields"][] = "shipping_agent_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agent_id", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "shipping_agent_id";	
}
$tdatatransaction[".globSearchFields"][] = "shipping_agent_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agent_name", $tdatatransaction[".allSearchFields"]))
{
	$tdatatransaction[".allSearchFields"][] = "shipping_agent_name";	
}


$tdatatransaction[".googleLikeFields"][] = "trx_id";
$tdatatransaction[".googleLikeFields"][] = "depo_id";
$tdatatransaction[".googleLikeFields"][] = "trx_type_id";
$tdatatransaction[".googleLikeFields"][] = "doc_number";
$tdatatransaction[".googleLikeFields"][] = "vessel_name";
$tdatatransaction[".googleLikeFields"][] = "vessel_voyage_id";
$tdatatransaction[".googleLikeFields"][] = "vessel_id";
$tdatatransaction[".googleLikeFields"][] = "customer_name";
$tdatatransaction[".googleLikeFields"][] = "shipping_agent_id";
$tdatatransaction[".googleLikeFields"][] = "shipping_agent_name";



$tdatatransaction[".advSearchFields"][] = "trx_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_id", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "trx_id";	
}
$tdatatransaction[".advSearchFields"][] = "depo_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("depo_id", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "depo_id";	
}
$tdatatransaction[".advSearchFields"][] = "trx_type_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_type_id", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "trx_type_id";	
}
$tdatatransaction[".advSearchFields"][] = "doc_number";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("doc_number", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "doc_number";	
}
$tdatatransaction[".advSearchFields"][] = "vessel_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("vessel_name", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "vessel_name";	
}
$tdatatransaction[".advSearchFields"][] = "vessel_voyage_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("vessel_voyage_id", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "vessel_voyage_id";	
}
$tdatatransaction[".advSearchFields"][] = "vessel_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("vessel_id", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "vessel_id";	
}
$tdatatransaction[".advSearchFields"][] = "customer_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("customer_name", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "customer_name";	
}
$tdatatransaction[".advSearchFields"][] = "shipping_agent_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agent_id", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "shipping_agent_id";	
}
$tdatatransaction[".advSearchFields"][] = "shipping_agent_name";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("shipping_agent_name", $tdatatransaction[".allSearchFields"])) 
{
	$tdatatransaction[".allSearchFields"][] = "shipping_agent_name";	
}

$tdatatransaction[".isTableType"] = "list";


	



// Access doesn't support subqueries from the same table as main
$tdatatransaction[".subQueriesSupAccess"] = true;

		


$tdatatransaction[".totalsFields"][] = array("fName"=>"trx_id", "totalsType"=>"COUNT", "viewFormat"=>"");
$tdatatransaction[".totalsFields"][] = array("fName"=>"depo_id", "totalsType"=>"COUNT", "viewFormat"=>"");

$tdatatransaction[".pageSize"] = 20;

$gstrOrderBy = "";
if(strlen($gstrOrderBy) && strtolower(substr($gstrOrderBy,0,8))!="order by")
	$gstrOrderBy = "order by ".$gstrOrderBy;
$tdatatransaction[".strOrderBy"] = $gstrOrderBy;
	
$tdatatransaction[".orderindexes"] = array();

$tdatatransaction[".sqlHead"] = "SELECT trx_id,  depo_id,  trx_type_id,  doc_number,  vessel_name,  vessel_voyage_id,  vessel_id,  customer_name,  shipping_agent_id,  shipping_agent_name";
$tdatatransaction[".sqlFrom"] = "FROM `transaction`";
$tdatatransaction[".sqlWhereExpr"] = "";
$tdatatransaction[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatatransaction[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatatransaction[".arrGroupsPerPage"] = $arrGPP;

	$tableKeys = array();
	$tableKeys[] = "trx_id";
	$tdatatransaction[".Keys"] = $tableKeys;

//	trx_id
	$fdata = array();
	$fdata["strName"] = "trx_id";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Trx Id"; 
	
		
		
	$fdata["FieldType"]= 3;
	
		$fdata["AutoInc"]=true;
	
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "trx_id";
	
		$fdata["FullName"]= "trx_id";
	
		$fdata["IsRequired"]=true; 
	
		
		
		
		
				$fdata["Index"]= 1;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
				$fdata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$fdata["validateAs"]["basicValidate"][] = "IsRequired";
	
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["trx_id"]=$fdata;
//	depo_id
	$fdata = array();
	$fdata["strName"] = "depo_id";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Depo Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "depo_id";
	
		$fdata["FullName"]= "depo_id";
	
		
		
		
		
		
				$fdata["Index"]= 2;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=100";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["depo_id"]=$fdata;
//	trx_type_id
	$fdata = array();
	$fdata["strName"] = "trx_type_id";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Trx Type Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "trx_type_id";
	
		$fdata["FullName"]= "trx_type_id";
	
		
		
		
		
		
				$fdata["Index"]= 3;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["trx_type_id"]=$fdata;
//	doc_number
	$fdata = array();
	$fdata["strName"] = "doc_number";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Doc Number"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "doc_number";
	
		$fdata["FullName"]= "doc_number";
	
		
		
		
		
		
				$fdata["Index"]= 4;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["doc_number"]=$fdata;
//	vessel_name
	$fdata = array();
	$fdata["strName"] = "vessel_name";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Vessel Name"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "vessel_name";
	
		$fdata["FullName"]= "vessel_name";
	
		
		
		
		
		
				$fdata["Index"]= 5;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["vessel_name"]=$fdata;
//	vessel_voyage_id
	$fdata = array();
	$fdata["strName"] = "vessel_voyage_id";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Vessel Voyage Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "vessel_voyage_id";
	
		$fdata["FullName"]= "vessel_voyage_id";
	
		
		
		
		
		
				$fdata["Index"]= 6;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["vessel_voyage_id"]=$fdata;
//	vessel_id
	$fdata = array();
	$fdata["strName"] = "vessel_id";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Vessel Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "vessel_id";
	
		$fdata["FullName"]= "vessel_id";
	
		
		
		
		
		
				$fdata["Index"]= 7;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["vessel_id"]=$fdata;
//	customer_name
	$fdata = array();
	$fdata["strName"] = "customer_name";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Customer Name"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "customer_name";
	
		$fdata["FullName"]= "customer_name";
	
		
		
		
		
		
				$fdata["Index"]= 8;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["customer_name"]=$fdata;
//	shipping_agent_id
	$fdata = array();
	$fdata["strName"] = "shipping_agent_id";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Shipping Agent Id"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "shipping_agent_id";
	
		$fdata["FullName"]= "shipping_agent_id";
	
		
		
		
		
		
				$fdata["Index"]= 9;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["shipping_agent_id"]=$fdata;
//	shipping_agent_name
	$fdata = array();
	$fdata["strName"] = "shipping_agent_name";
	$fdata["ownerTable"] = "transaction";
	$fdata["Label"]="Shipping Agent Name"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "shipping_agent_name";
	
		$fdata["FullName"]= "shipping_agent_name";
	
		
		
		
		
		
				$fdata["Index"]= 10;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		$fdata["bExportPage"]=true; 
	
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatatransaction["shipping_agent_name"]=$fdata;


	
$tables_data["transaction"]=&$tdatatransaction;
$field_labels["transaction"] = &$fieldLabelstransaction;
$fieldToolTips["transaction"] = &$fieldToolTipstransaction;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["transaction"] = array();
$dIndex = 1-1;
			$strOriginalDetailsTable="invoice";
	$detailsTablesData["transaction"][$dIndex] = array(
		  "dDataSourceTable"=>"invoice"
		, "dOriginalTable"=>$strOriginalDetailsTable
		, "dShortTable"=>"invoice"
		, "masterKeys"=>array()
		, "detailKeys"=>array()
		, "dispChildCount"=> "1"
		, "hideChild"=>"0"
		, "sqlHead"=>"SELECT invoice_nbr,  trx_id,  iso_code,  qty_of_20,  qty_of_40,  total,  created,  creator"	
		, "sqlFrom"=>"FROM invoice"	
		, "sqlWhere"=>""
		, "sqlTail"=>""
		, "groupBy"=>"0"
		, "previewOnList" => 1
		, "previewOnAdd" => 0
		, "previewOnEdit" => 0
		, "previewOnView" => 1
	);	
		$detailsTablesData["transaction"][$dIndex]["masterKeys"][]="trx_id";
		$detailsTablesData["transaction"][$dIndex]["detailKeys"][]="trx_id";

$dIndex = 2-1;
			$strOriginalDetailsTable="equipment_uses";
	$detailsTablesData["transaction"][$dIndex] = array(
		  "dDataSourceTable"=>"equipment_uses"
		, "dOriginalTable"=>$strOriginalDetailsTable
		, "dShortTable"=>"equipment_uses"
		, "masterKeys"=>array()
		, "detailKeys"=>array()
		, "dispChildCount"=> "1"
		, "hideChild"=>"0"
		, "sqlHead"=>"SELECT gkey,  eq_nbr,  line_id,  depo_id,  shipping_agents_id,  shipping_agent_name,  creator,  created,  port_of_origin,  consignee,  port_of_destination,  bl_nbr,  category,  document_verfied,  survey_pos_id,  ctr_position,  iso_code,  eq_size,  eq_type,  shipping_line_name,  tare_weight_unit,  payment_release,  trx_id"	
		, "sqlFrom"=>"FROM equipment_uses"	
		, "sqlWhere"=>""
		, "sqlTail"=>""
		, "groupBy"=>"0"
		, "previewOnList" => 0
		, "previewOnAdd" => 0
		, "previewOnEdit" => 0
		, "previewOnView" => 1
	);	
		$detailsTablesData["transaction"][$dIndex]["masterKeys"][]="trx_id";
		$detailsTablesData["transaction"][$dIndex]["detailKeys"][]="trx_id";


	
// tables which are master tables for current table (detail)
$masterTablesData["transaction"] = array();

// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_transaction()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "trx_id,  depo_id,  trx_type_id,  doc_number,  vessel_name,  vessel_voyage_id,  vessel_id,  customer_name,  shipping_agent_id,  shipping_agent_name";
$proto0["m_strFrom"] = "FROM `transaction`";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = "0";
$proto1["m_inBrackets"] = "0";
$proto1["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = "0";
$proto3["m_inBrackets"] = "0";
$proto3["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "trx_id",
	"m_strTable" => "transaction"
));

$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "depo_id",
	"m_strTable" => "transaction"
));

$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "trx_type_id",
	"m_strTable" => "transaction"
));

$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "doc_number",
	"m_strTable" => "transaction"
));

$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "vessel_name",
	"m_strTable" => "transaction"
));

$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "vessel_voyage_id",
	"m_strTable" => "transaction"
));

$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "vessel_id",
	"m_strTable" => "transaction"
));

$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "customer_name",
	"m_strTable" => "transaction"
));

$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "shipping_agent_id",
	"m_strTable" => "transaction"
));

$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto0["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "shipping_agent_name",
	"m_strTable" => "transaction"
));

$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto25=array();
$proto25["m_link"] = "SQLL_MAIN";
			$proto26=array();
$proto26["m_strName"] = "transaction";
$proto26["m_columns"] = array();
$proto26["m_columns"][] = "trx_id";
$proto26["m_columns"][] = "customer_tax_id";
$proto26["m_columns"][] = "customer_name";
$proto26["m_columns"][] = "vessel_id";
$proto26["m_columns"][] = "vessel_voyage_id";
$proto26["m_columns"][] = "vessel_name";
$proto26["m_columns"][] = "doc_number";
$proto26["m_columns"][] = "trx_type_id";
$proto26["m_columns"][] = "trx_type_name";
$proto26["m_columns"][] = "created";
$proto26["m_columns"][] = "creator";
$proto26["m_columns"][] = "changed";
$proto26["m_columns"][] = "changer";
$proto26["m_columns"][] = "start_timestamp";
$proto26["m_columns"][] = "end_timestamp";
$proto26["m_columns"][] = "bat_nbr";
$proto26["m_columns"][] = "shipping_agent_id";
$proto26["m_columns"][] = "shipping_agent_name";
$proto26["m_columns"][] = "trucking_company_id";
$proto26["m_columns"][] = "trucking_company_name";
$proto26["m_columns"][] = "depo_release";
$proto26["m_columns"][] = "payment_release";
$proto26["m_columns"][] = "depo_id";
$proto26["m_columns"][] = "truck_license_nbr";
$obj = new SQLTable($proto26);

$proto25["m_table"] = $obj;
$proto25["m_alias"] = "";
$proto27=array();
$proto27["m_sql"] = "";
$proto27["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto27["m_column"]=$obj;
$proto27["m_contained"] = array();
$proto27["m_strCase"] = "";
$proto27["m_havingmode"] = "0";
$proto27["m_inBrackets"] = "0";
$proto27["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto27);

$proto25["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto25);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$obj = new SQLQuery($proto0);

return $obj;
}
$queryData_transaction = createSqlQuery_transaction();
$tdatatransaction[".sqlquery"] = $queryData_transaction;



$tableEvents["transaction"] = new eventsBase;
$tdatatransaction[".hasEvents"] = false;

?>
