<?php
$tdatainvoice=array();
	$tdatainvoice[".NumberOfChars"]=80; 
	$tdatainvoice[".ShortName"]="invoice";
	$tdatainvoice[".OwnerID"]="";
	$tdatainvoice[".OriginalTable"]="invoice";


	
//	field labels
$fieldLabelsinvoice = array();
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelsinvoice["English"]=array();
	$fieldToolTipsinvoice["English"]=array();
	$fieldLabelsinvoice["English"]["invoice_nbr"] = "Invoice Nbr";
	$fieldToolTipsinvoice["English"]["invoice_nbr"] = "";
	$fieldLabelsinvoice["English"]["trx_id"] = "Trx Id";
	$fieldToolTipsinvoice["English"]["trx_id"] = "";
	$fieldLabelsinvoice["English"]["iso_code"] = "Iso Code";
	$fieldToolTipsinvoice["English"]["iso_code"] = "";
	$fieldLabelsinvoice["English"]["qty_of_20"] = "Qty Of 20";
	$fieldToolTipsinvoice["English"]["qty_of_20"] = "";
	$fieldLabelsinvoice["English"]["qty_of_40"] = "Qty Of 40";
	$fieldToolTipsinvoice["English"]["qty_of_40"] = "";
	$fieldLabelsinvoice["English"]["total"] = "Total";
	$fieldToolTipsinvoice["English"]["total"] = "";
	$fieldLabelsinvoice["English"]["created"] = "Created";
	$fieldToolTipsinvoice["English"]["created"] = "";
	$fieldLabelsinvoice["English"]["creator"] = "Creator";
	$fieldToolTipsinvoice["English"]["creator"] = "";
	if (count($fieldToolTipsinvoice["English"])){
		$tdatainvoice[".isUseToolTips"]=true;
	}
}


	
	$tdatainvoice[".NCSearch"]=true;

	

$tdatainvoice[".shortTableName"] = "invoice";
$tdatainvoice[".nSecOptions"] = 0;
$tdatainvoice[".recsPerRowList"] = 1;	
$tdatainvoice[".tableGroupBy"] = "0";
$tdatainvoice[".mainTableOwnerID"] = "";
$tdatainvoice[".moveNext"] = 1;




$tdatainvoice[".showAddInPopup"] = false;

$tdatainvoice[".showEditInPopup"] = false;

$tdatainvoice[".showViewInPopup"] = false;


$tdatainvoice[".fieldsForRegister"] = array();

$tdatainvoice[".listAjax"] = false;

	$tdatainvoice[".audit"] = false;

	$tdatainvoice[".locking"] = false;
	
$tdatainvoice[".listIcons"] = true;
$tdatainvoice[".view"] = true;


$tdatainvoice[".printFriendly"] = true;


$tdatainvoice[".showSimpleSearchOptions"] = false;

$tdatainvoice[".showSearchPanel"] = true;


$tdatainvoice[".isUseAjaxSuggest"] = true;

$tdatainvoice[".rowHighlite"] = true;


// button handlers file names

$tdatainvoice[".addPageEvents"] = false;

$tdatainvoice[".arrKeyFields"][] = "trx_id";

// use datepicker for search panel
$tdatainvoice[".isUseCalendarForSearch"] = true;

// use timepicker for search panel
$tdatainvoice[".isUseTimeForSearch"] = false;

$tdatainvoice[".isUseiBox"] = false;


	

	



$tdatainvoice[".isUseInlineJs"] = $tdatainvoice[".isUseInlineAdd"] || $tdatainvoice[".isUseInlineEdit"];

$tdatainvoice[".allSearchFields"] = array();

$tdatainvoice[".globSearchFields"][] = "invoice_nbr";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("invoice_nbr", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "invoice_nbr";	
}
$tdatainvoice[".globSearchFields"][] = "trx_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_id", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "trx_id";	
}
$tdatainvoice[".globSearchFields"][] = "iso_code";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("iso_code", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "iso_code";	
}
$tdatainvoice[".globSearchFields"][] = "qty_of_20";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("qty_of_20", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "qty_of_20";	
}
$tdatainvoice[".globSearchFields"][] = "qty_of_40";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("qty_of_40", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "qty_of_40";	
}
$tdatainvoice[".globSearchFields"][] = "total";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("total", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "total";	
}
$tdatainvoice[".globSearchFields"][] = "created";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("created", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "created";	
}
$tdatainvoice[".globSearchFields"][] = "creator";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("creator", $tdatainvoice[".allSearchFields"]))
{
	$tdatainvoice[".allSearchFields"][] = "creator";	
}


$tdatainvoice[".googleLikeFields"][] = "invoice_nbr";
$tdatainvoice[".googleLikeFields"][] = "trx_id";
$tdatainvoice[".googleLikeFields"][] = "iso_code";
$tdatainvoice[".googleLikeFields"][] = "qty_of_20";
$tdatainvoice[".googleLikeFields"][] = "qty_of_40";
$tdatainvoice[".googleLikeFields"][] = "total";
$tdatainvoice[".googleLikeFields"][] = "created";
$tdatainvoice[".googleLikeFields"][] = "creator";



$tdatainvoice[".advSearchFields"][] = "invoice_nbr";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("invoice_nbr", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "invoice_nbr";	
}
$tdatainvoice[".advSearchFields"][] = "trx_id";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("trx_id", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "trx_id";	
}
$tdatainvoice[".advSearchFields"][] = "iso_code";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("iso_code", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "iso_code";	
}
$tdatainvoice[".advSearchFields"][] = "qty_of_20";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("qty_of_20", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "qty_of_20";	
}
$tdatainvoice[".advSearchFields"][] = "qty_of_40";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("qty_of_40", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "qty_of_40";	
}
$tdatainvoice[".advSearchFields"][] = "total";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("total", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "total";	
}
$tdatainvoice[".advSearchFields"][] = "created";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("created", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "created";	
}
$tdatainvoice[".advSearchFields"][] = "creator";
// do in this way, because combine functions array_unique and array_merge returns array with keys like 1,2, 4 etc
if (!in_array("creator", $tdatainvoice[".allSearchFields"])) 
{
	$tdatainvoice[".allSearchFields"][] = "creator";	
}

$tdatainvoice[".isTableType"] = "list";


	



// Access doesn't support subqueries from the same table as main
$tdatainvoice[".subQueriesSupAccess"] = true;




$tdatainvoice[".totalsFields"][] = array("fName"=>"trx_id", "totalsType"=>"COUNT", "viewFormat"=>"");
$tdatainvoice[".totalsFields"][] = array("fName"=>"qty_of_20", "totalsType"=>"TOTAL", "viewFormat"=>"");
$tdatainvoice[".totalsFields"][] = array("fName"=>"qty_of_40", "totalsType"=>"TOTAL", "viewFormat"=>"");
$tdatainvoice[".totalsFields"][] = array("fName"=>"total", "totalsType"=>"TOTAL", "viewFormat"=>"");

$tdatainvoice[".pageSize"] = 20;

$gstrOrderBy = "";
if(strlen($gstrOrderBy) && strtolower(substr($gstrOrderBy,0,8))!="order by")
	$gstrOrderBy = "order by ".$gstrOrderBy;
$tdatainvoice[".strOrderBy"] = $gstrOrderBy;
	
$tdatainvoice[".orderindexes"] = array();

$tdatainvoice[".sqlHead"] = "SELECT invoice_nbr,  trx_id,  iso_code,  qty_of_20,  qty_of_40,  total,  created,  creator";
$tdatainvoice[".sqlFrom"] = "FROM invoice";
$tdatainvoice[".sqlWhereExpr"] = "";
$tdatainvoice[".sqlTail"] = "";




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatainvoice[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatainvoice[".arrGroupsPerPage"] = $arrGPP;

	$tableKeys = array();
	$tableKeys[] = "trx_id";
	$tdatainvoice[".Keys"] = $tableKeys;

//	invoice_nbr
	$fdata = array();
	$fdata["strName"] = "invoice_nbr";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Invoice Nbr"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "invoice_nbr";
	
		$fdata["FullName"]= "invoice_nbr";
	
		
		
		
		
		
				$fdata["Index"]= 1;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=255";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["invoice_nbr"]=$fdata;
//	trx_id
	$fdata = array();
	$fdata["strName"] = "trx_id";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Trx Id"; 
	
		
		
	$fdata["FieldType"]= 3;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "trx_id";
	
		$fdata["FullName"]= "trx_id";
	
		$fdata["IsRequired"]=true; 
	
		
		
		
		
				$fdata["Index"]= 2;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
				$fdata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						$fdata["validateAs"]["basicValidate"][] = "IsRequired";
	
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["trx_id"]=$fdata;
//	iso_code
	$fdata = array();
	$fdata["strName"] = "iso_code";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Iso Code"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "iso_code";
	
		$fdata["FullName"]= "iso_code";
	
		
		
		
		
		
				$fdata["Index"]= 3;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=100";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["iso_code"]=$fdata;
//	qty_of_20
	$fdata = array();
	$fdata["strName"] = "qty_of_20";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Qty Of 20"; 
	
		
		
	$fdata["FieldType"]= 3;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "qty_of_20";
	
		$fdata["FullName"]= "qty_of_20";
	
		
		
		
		
		
				$fdata["Index"]= 4;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
				$fdata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["qty_of_20"]=$fdata;
//	qty_of_40
	$fdata = array();
	$fdata["strName"] = "qty_of_40";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Qty Of 40"; 
	
		
		
	$fdata["FieldType"]= 3;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "qty_of_40";
	
		$fdata["FullName"]= "qty_of_40";
	
		
		
		
		
		
				$fdata["Index"]= 5;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
				$fdata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["qty_of_40"]=$fdata;
//	total
	$fdata = array();
	$fdata["strName"] = "total";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Total"; 
	
		
		
	$fdata["FieldType"]= 3;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "total";
	
		$fdata["FullName"]= "total";
	
		
		
		
		
		
				$fdata["Index"]= 6;
				$fdata["EditParams"]="";
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
				$fdata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");	
						
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["total"]=$fdata;
//	created
	$fdata = array();
	$fdata["strName"] = "created";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Created"; 
	
		
		
	$fdata["FieldType"]= 135;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Date";
	$fdata["ViewFormat"]= "Short Date";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "created";
	
		$fdata["FullName"]= "created";
	
		$fdata["IsRequired"]=true; 
	
		
		
		
		
				$fdata["Index"]= 7;
		$fdata["DateEditType"] = 13; 
	$fdata["InitialYearFactor"] = 100; 
	$fdata["LastYearFactor"] = 10; 
			
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
						$fdata["validateAs"]["basicValidate"][] = "IsRequired";
	
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["created"]=$fdata;
//	creator
	$fdata = array();
	$fdata["strName"] = "creator";
	$fdata["ownerTable"] = "invoice";
	$fdata["Label"]="Creator"; 
	
		
		
	$fdata["FieldType"]= 200;
	
		
			$fdata["UseiBox"] = false;
	
	$fdata["EditFormat"]= "Text field";
	$fdata["ViewFormat"]= "";
	
		
		
		
		
		$fdata["NeedEncode"]=true;
	
	$fdata["GoodName"]= "creator";
	
		$fdata["FullName"]= "creator";
	
		
		
		
		
		
				$fdata["Index"]= 8;
				$fdata["EditParams"]="";
			$fdata["EditParams"].= " maxlength=255";
		
		$fdata["bListPage"]=true; 
	
		
		
		
		
		$fdata["bViewPage"]=true; 
	
		$fdata["bAdvancedSearch"]=true; 
	
		$fdata["bPrinterPage"]=true; 
	
		
	//Begin validation
	$fdata["validateAs"] = array();
		
		//End validation
	
				$fdata["FieldPermissions"]=true;
	
		
				
		
		
		
			$tdatainvoice["creator"]=$fdata;


	
$tables_data["invoice"]=&$tdatainvoice;
$field_labels["invoice"] = &$fieldLabelsinvoice;
$fieldToolTips["invoice"] = &$fieldToolTipsinvoice;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["invoice"] = array();

	
// tables which are master tables for current table (detail)
$masterTablesData["invoice"] = array();

$mIndex = 1-1;
			$strOriginalDetailsTable="transaction";
	$masterTablesData["invoice"][$mIndex] = array(
		  "mDataSourceTable"=>"transaction"
		, "mOriginalTable" => $strOriginalDetailsTable
		, "mShortTable" => "transaction"
		, "masterKeys" => array()
		, "detailKeys" => array()
		, "dispChildCount" => "1"
		, "hideChild" => "0"	
		, "dispInfo" => "1"								
		, "previewOnList" => 1
		, "previewOnAdd" => 0
		, "previewOnEdit" => 0
		, "previewOnView" => 1
	);	
		$masterTablesData["invoice"][$mIndex]["masterKeys"][]="trx_id";
		$masterTablesData["invoice"][$mIndex]["detailKeys"][]="trx_id";

// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_invoice()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "invoice_nbr,  trx_id,  iso_code,  qty_of_20,  qty_of_40,  total,  created,  creator";
$proto0["m_strFrom"] = "FROM invoice";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
$proto0["m_strTail"] = "";
$proto1=array();
$proto1["m_sql"] = "";
$proto1["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto1["m_column"]=$obj;
$proto1["m_contained"] = array();
$proto1["m_strCase"] = "";
$proto1["m_havingmode"] = "0";
$proto1["m_inBrackets"] = "0";
$proto1["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto1);

$proto0["m_where"] = $obj;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = "0";
$proto3["m_inBrackets"] = "0";
$proto3["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto3);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto5=array();
			$obj = new SQLField(array(
	"m_strName" => "invoice_nbr",
	"m_strTable" => "invoice"
));

$proto5["m_expr"]=$obj;
$proto5["m_alias"] = "";
$obj = new SQLFieldListItem($proto5);

$proto0["m_fieldlist"][]=$obj;
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "trx_id",
	"m_strTable" => "invoice"
));

$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto0["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "iso_code",
	"m_strTable" => "invoice"
));

$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto0["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "qty_of_20",
	"m_strTable" => "invoice"
));

$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto0["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "qty_of_40",
	"m_strTable" => "invoice"
));

$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto0["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "total",
	"m_strTable" => "invoice"
));

$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto0["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "created",
	"m_strTable" => "invoice"
));

$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto0["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "creator",
	"m_strTable" => "invoice"
));

$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto21=array();
$proto21["m_link"] = "SQLL_MAIN";
			$proto22=array();
$proto22["m_strName"] = "invoice";
$proto22["m_columns"] = array();
$proto22["m_columns"][] = "invoice_nbr";
$proto22["m_columns"][] = "trx_id";
$proto22["m_columns"][] = "iso_code";
$proto22["m_columns"][] = "qty_of_20";
$proto22["m_columns"][] = "qty_of_40";
$proto22["m_columns"][] = "total";
$proto22["m_columns"][] = "created";
$proto22["m_columns"][] = "creator";
$obj = new SQLTable($proto22);

$proto21["m_table"] = $obj;
$proto21["m_alias"] = "";
$proto23=array();
$proto23["m_sql"] = "";
$proto23["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto23["m_column"]=$obj;
$proto23["m_contained"] = array();
$proto23["m_strCase"] = "";
$proto23["m_havingmode"] = "0";
$proto23["m_inBrackets"] = "0";
$proto23["m_useAlias"] = "0";
$obj = new SQLLogicalExpr($proto23);

$proto21["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto21);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$obj = new SQLQuery($proto0);

return $obj;
}
$queryData_invoice = createSqlQuery_invoice();
$tdatainvoice[".sqlquery"] = $queryData_invoice;



$tableEvents["invoice"] = new eventsBase;
$tdatainvoice[".hasEvents"] = false;

?>
