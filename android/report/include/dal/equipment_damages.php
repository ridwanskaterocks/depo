<?php
$dalTableequipment_damages = array();
$dalTableequipment_damages["id_damage"] = array("type"=>3,"varname"=>"id_damage");
$dalTableequipment_damages["equse_gkey"] = array("type"=>3,"varname"=>"equse_gkey");
$dalTableequipment_damages["eq_nbr"] = array("type"=>200,"varname"=>"eq_nbr");
$dalTableequipment_damages["location"] = array("type"=>200,"varname"=>"location");
$dalTableequipment_damages["component"] = array("type"=>200,"varname"=>"component");
$dalTableequipment_damages["other_damage_code"] = array("type"=>200,"varname"=>"other_damage_code");
$dalTableequipment_damages["other_damage_desc"] = array("type"=>200,"varname"=>"other_damage_desc");
$dalTableequipment_damages["location_component"] = array("type"=>200,"varname"=>"location_component");
$dalTableequipment_damages["bent_flag"] = array("type"=>200,"varname"=>"bent_flag");
$dalTableequipment_damages["Dentet_flag"] = array("type"=>200,"varname"=>"Dentet_flag");
$dalTableequipment_damages["Leaking_flag"] = array("type"=>200,"varname"=>"Leaking_flag");
$dalTableequipment_damages["PushIn_flag"] = array("type"=>200,"varname"=>"PushIn_flag");
$dalTableequipment_damages["Broke_flag"] = array("type"=>200,"varname"=>"Broke_flag");
$dalTableequipment_damages["Hole_flag"] = array("type"=>200,"varname"=>"Hole_flag");
$dalTableequipment_damages["Missing_flag"] = array("type"=>200,"varname"=>"Missing_flag");
$dalTableequipment_damages["PushOut_flag"] = array("type"=>200,"varname"=>"PushOut_flag");
$dalTableequipment_damages["Cut_flag"] = array("type"=>200,"varname"=>"Cut_flag");
$dalTableequipment_damages["Loose_flag"] = array("type"=>200,"varname"=>"Loose_flag");
$dalTableequipment_damages["Tom_flag"] = array("type"=>200,"varname"=>"Tom_flag");
$dalTableequipment_damages["Rusty_flag"] = array("type"=>200,"varname"=>"Rusty_flag");
$dalTableequipment_damages["survey_location"] = array("type"=>200,"varname"=>"survey_location");
$dalTableequipment_damages["InGatephoto"] = array("type"=>128,"varname"=>"InGatephoto");
$dalTableequipment_damages["OutGatePhoto"] = array("type"=>128,"varname"=>"OutGatePhoto");
$dalTableequipment_damages["created"] = array("type"=>135,"varname"=>"created");
$dalTableequipment_damages["creator"] = array("type"=>200,"varname"=>"creator");
$dalTableequipment_damages["changed"] = array("type"=>135,"varname"=>"changed");
$dalTableequipment_damages["changer"] = array("type"=>200,"varname"=>"changer");
$dalTableequipment_damages["id"] = array("type"=>200,"varname"=>"id");
$dalTableequipment_damages["photoinstring"] = array("type"=>201,"varname"=>"photoinstring");
$dalTableequipment_damages["filename"] = array("type"=>200,"varname"=>"filename");
$dalTableequipment_damages["depo_id"] = array("type"=>200,"varname"=>"depo_id");
	$dalTableequipment_damages["id_damage"]["key"]=true;
$dal_info["equipment_damages"]=&$dalTableequipment_damages;

?>