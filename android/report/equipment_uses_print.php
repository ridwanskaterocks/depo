<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

include("include/dbcommon.php");
include("classes/searchclause.php");

add_nocache_headers();

include("include/equipment_uses_variables.php");

if(!@$_SESSION["UserID"])
{ 
	$_SESSION["MyURL"]=$_SERVER["SCRIPT_NAME"]."?".$_SERVER["QUERY_STRING"];
	header("Location: login.php?message=expired"); 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Export"))
{
	echo "<p>"."You don't have permissions to access this table"."<a href=\"login.php\">"."Back to login page"."</a></p>";
	return;
}

$layout = new TLayout("print","BoldOrange","MobileOrange");
$layout->blocks["center"] = array();
$layout->containers["grid"] = array();

$layout->containers["grid"][] = array("name"=>"printgrid","block"=>"grid_block","substyle"=>1);


$layout->skins["grid"] = "empty";
$layout->blocks["center"][] = "grid";$layout->blocks["top"] = array();
$layout->containers["master"] = array();

$layout->containers["master"][] = array("name"=>"masterinfoprint","block"=>"mastertable_block","substyle"=>1);


$layout->skins["master"] = "empty";
$layout->blocks["top"][] = "master";
$layout->skins["pdf"] = "empty";
$layout->blocks["top"][] = "pdf";$page_layouts["equipment_uses_print"] = $layout;


include('include/xtempl.php');
include('classes/runnerpage.php');

$xt = new Xtempl();
$id = postvalue("id") != "" ? postvalue("id") : 1;
$all = postvalue("all");
$pageName = "print.php";

//array of params for classes
$params = array("pageType" => PAGE_PRINT, 
				"id" =>$id, 
				"tName"=>$strTableName);
$params["xt"] = &$xt;
	
$pageObject = new RunnerPage($params);

// add button events if exist
$pageObject->addButtonHandlers();

// Modify query: remove blob fields from fieldlist.
// Blob fields on a print page are shown using imager.php (for example).
// They don't need to be selected from DB in print.php itself.
if(!postvalue("pdf"))
	$gQuery->ReplaceFieldsWithDummies(GetBinaryFieldsIndices());

//	Before Process event
if($eventObj->exists("BeforeProcessPrint"))
	$eventObj->BeforeProcessPrint($conn);

$strWhereClause="";
$strHavingClause="";

$selected_recs=array();
if (@$_REQUEST["a"]!="") 
{
	$sWhere = "1=0";	
	
//	process selection
	if (@$_REQUEST["mdelete"])
	{
		foreach(@$_REQUEST["mdelete"] as $ind)
		{
			$keys=array();
			$keys["gkey"]=refine($_REQUEST["mdelete1"][mdeleteIndex($ind)]);
			$selected_recs[]=$keys;
		}
	}
	elseif(@$_REQUEST["selection"])
	{
		foreach(@$_REQUEST["selection"] as $keyblock)
		{
			$arr=explode("&",refine($keyblock));
			if(count($arr)<1)
				continue;
			$keys=array();
			$keys["gkey"]=urldecode($arr[0]);
			$selected_recs[]=$keys;
		}
	}

	foreach($selected_recs as $keys)
	{
		$sWhere = $sWhere . " or ";
		$sWhere.=KeyWhere($keys);
	}
	$strSQL = gSQLWhere($sWhere);
	$strWhereClause=$sWhere;
}
else
{
	$strWhereClause=@$_SESSION[$strTableName."_where"];
	$strHavingClause=@$_SESSION[$strTableName."_having"];
	$strSQL = gSQLWhere($strWhereClause, $strHavingClause);
}
if(postvalue("pdf"))
	$strWhereClause = @$_SESSION[$strTableName."_pdfwhere"];

$_SESSION[$strTableName."_pdfwhere"] = $strWhereClause;


$strOrderBy=$_SESSION[$strTableName."_order"];
if(!$strOrderBy)
	$strOrderBy=$gstrOrderBy;
$strSQL.=" ".trim($strOrderBy);

$strSQLbak = $strSQL;
if($eventObj->exists("BeforeQueryPrint"))
	$eventObj->BeforeQueryPrint($strSQL,$strWhereClause,$strOrderBy);

//	Rebuild SQL if needed

if($strSQL!=$strSQLbak)
{
//	changed $strSQL - old style	
	$numrows=GetRowCount($strSQL);
}
else
{
	$strSQL = gSQLWhere($strWhereClause, $strHavingClause);
	$strSQL.=" ".trim($strOrderBy);
	
	$rowcount=false;
	if($eventObj->exists("ListGetRowCount"))
	{
		$masterKeysReq=array();
		for($i = 0; $i < count($pageObject->detailKeysByM); $i ++)
			$masterKeysReq[]=$_SESSION[$strTableName."_masterkey".($i + 1)];
			$rowcount=$eventObj->ListGetRowCount($pageObject->searchClauseObj,$_SESSION[$strTableName."_mastertable"],$masterKeysReq,$selected_recs);
	}
	if($rowcount!==false)
		$numrows=$rowcount;
	else
	{
		$numrows = gSQLRowCount($strWhereClause, $strHavingClause);
	}
}

LogInfo($strSQL);

$mypage=(integer)$_SESSION[$strTableName."_pagenumber"];
if(!$mypage)
	$mypage=1;

//	page size
$PageSize=(integer)$_SESSION[$strTableName."_pagesize"];
if(!$PageSize)
	$PageSize = GetTableData($strTableName,".pageSize",0);

if($PageSize<0)
	$all = 1;	
	
$recno = 1;
$records = 0;	
$maxpages = 1;
$pageindex = 1;
$pageno=1;

if(!$all)
{	
	if($numrows)
	{
		$maxRecords = $numrows;
		$maxpages = ceil($maxRecords/$PageSize);
					
		if($mypage > $maxpages)
			$mypage = $maxpages;
		
		if($mypage < 1) 
			$mypage = 1;
		
		$maxrecs = $PageSize;
	}
	$listarray = false;
	if($eventObj->exists("ListQuery"))
		$listarray = $eventObj->ListQuery($pageObject->searchClauseObj,$_SESSION[$strTableName."_arrFieldForSort"],$_SESSION[$strTableName."_arrHowFieldSort"],$_SESSION[$strTableName."_mastertable"],$masterKeysReq,$selected_recs,$PageSize,$mypage);
	if($listarray!==false)
		$rs = $listarray;
	else
	{
			if($numrows)
		{
			$strSQL.=" limit ".(($mypage-1)*$PageSize).",".$PageSize;
		}
		$rs = db_query($strSQL,$conn);
	}
	
	//	hide colunm headers if needed
	$recordsonpage = $numrows-($mypage-1)*$PageSize;
	if($recordsonpage>$PageSize)
		$recordsonpage = $PageSize;
		
	$xt->assign("page_number",true);
	$xt->assign("maxpages",$maxpages);
	$xt->assign("pageno",$mypage);
}
else
{
	$listarray = false;
	if($eventObj->exists("ListQuery"))
		$listarray=$eventObj->ListQuery($pageObject->searchClauseObj,$_SESSION[$strTableName."_arrFieldForSort"],$_SESSION[$strTableName."_arrHowFieldSort"],$_SESSION[$strTableName."_mastertable"],$masterKeysReq,$selected_recs,$PageSize,$mypage);
	if($listarray!==false)
		$rs = $listarray;
	else
		$rs = db_query($strSQL,$conn);
	$recordsonpage = $numrows;
	$maxpages = ceil($recordsonpage/30);
	$xt->assign("page_number",true);
	$xt->assign("maxpages",$maxpages);
}


$fieldsArr = array();
$arr = array();
$arr['fName'] = "gkey";
$arr['viewFormat'] = ViewFormat("gkey", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "eq_nbr";
$arr['viewFormat'] = ViewFormat("eq_nbr", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "line_id";
$arr['viewFormat'] = ViewFormat("line_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "depo_id";
$arr['viewFormat'] = ViewFormat("depo_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "shipping_agents_id";
$arr['viewFormat'] = ViewFormat("shipping_agents_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "shipping_agent_name";
$arr['viewFormat'] = ViewFormat("shipping_agent_name", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "creator";
$arr['viewFormat'] = ViewFormat("creator", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "created";
$arr['viewFormat'] = ViewFormat("created", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "port_of_origin";
$arr['viewFormat'] = ViewFormat("port_of_origin", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "consignee";
$arr['viewFormat'] = ViewFormat("consignee", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "port_of_destination";
$arr['viewFormat'] = ViewFormat("port_of_destination", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "bl_nbr";
$arr['viewFormat'] = ViewFormat("bl_nbr", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "category";
$arr['viewFormat'] = ViewFormat("category", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "document_verfied";
$arr['viewFormat'] = ViewFormat("document_verfied", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "survey_pos_id";
$arr['viewFormat'] = ViewFormat("survey_pos_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "ctr_position";
$arr['viewFormat'] = ViewFormat("ctr_position", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "iso_code";
$arr['viewFormat'] = ViewFormat("iso_code", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "eq_size";
$arr['viewFormat'] = ViewFormat("eq_size", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "eq_type";
$arr['viewFormat'] = ViewFormat("eq_type", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "shipping_line_name";
$arr['viewFormat'] = ViewFormat("shipping_line_name", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "tare_weight_unit";
$arr['viewFormat'] = ViewFormat("tare_weight_unit", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "payment_release";
$arr['viewFormat'] = ViewFormat("payment_release", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "trx_id";
$arr['viewFormat'] = ViewFormat("trx_id", $strTableName);
$fieldsArr[] = $arr;
$pageObject->setGoogleMapsParams($fieldsArr);

$colsonpage=1;
if($colsonpage>$recordsonpage)
	$colsonpage=$recordsonpage;
if($colsonpage<1)
	$colsonpage=1;

	$totals=array();
	$totals["eq_nbr"]=0;

//	fill $rowinfo array
	$pages = array();
	$rowinfo = array();
	$rowinfo["data"]=array();
	if($eventObj->exists("ListFetchArray"))
		$data = $eventObj->ListFetchArray($rs);
	else
		$data = db_fetch_array($rs);	

	while($data)
	{
		if($eventObj->exists("BeforeProcessRowPrint"))
		{
			if(!$eventObj->BeforeProcessRowPrint($data))
			{
				if($eventObj->exists("ListFetchArray"))
					$data = $eventObj->ListFetchArray($rs);
				else
					$data = db_fetch_array($rs);
				continue;
			}
		}
		break;
	}
	
	while($data && ($all || $recno<=$PageSize))
	{
		$row=array();
		$row["grid_record"]=array();
		$row["grid_record"]["data"]=array();
		for($col=1;$data && ($all || $recno<=$PageSize) && $col<=1;$col++)
		{
			$record=array();
						$totals["eq_nbr"]+= ($data["eq_nbr"]!="");
			$recno++;
			$records++;
			$keylink="";
			$keylink.="&key1=".htmlspecialchars(rawurlencode(@$data["gkey"]));


//	gkey - 
			$value="";
				$value = ProcessLargeText(GetData($data,"gkey", ""),"field=gkey".$keylink,"",MODE_PRINT);
			$record["gkey_value"]=$value;

//	eq_nbr - 
			$value="";
				$value = ProcessLargeText(GetData($data,"eq_nbr", ""),"field=eq%5Fnbr".$keylink,"",MODE_PRINT);
			$record["eq_nbr_value"]=$value;

//	line_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"line_id", ""),"field=line%5Fid".$keylink,"",MODE_PRINT);
			$record["line_id_value"]=$value;

//	depo_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"depo_id", ""),"field=depo%5Fid".$keylink,"",MODE_PRINT);
			$record["depo_id_value"]=$value;

//	shipping_agents_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"shipping_agents_id", ""),"field=shipping%5Fagents%5Fid".$keylink,"",MODE_PRINT);
			$record["shipping_agents_id_value"]=$value;

//	shipping_agent_name - 
			$value="";
				$value = ProcessLargeText(GetData($data,"shipping_agent_name", ""),"field=shipping%5Fagent%5Fname".$keylink,"",MODE_PRINT);
			$record["shipping_agent_name_value"]=$value;

//	creator - 
			$value="";
				$value = ProcessLargeText(GetData($data,"creator", ""),"field=creator".$keylink,"",MODE_PRINT);
			$record["creator_value"]=$value;

//	created - Short Date
			$value="";
				$value = ProcessLargeText(GetData($data,"created", "Short Date"),"field=created".$keylink,"",MODE_PRINT);
			$record["created_value"]=$value;

//	port_of_origin - 
			$value="";
				$value = ProcessLargeText(GetData($data,"port_of_origin", ""),"field=port%5Fof%5Forigin".$keylink,"",MODE_PRINT);
			$record["port_of_origin_value"]=$value;

//	consignee - 
			$value="";
				$value = ProcessLargeText(GetData($data,"consignee", ""),"field=consignee".$keylink,"",MODE_PRINT);
			$record["consignee_value"]=$value;

//	port_of_destination - 
			$value="";
				$value = ProcessLargeText(GetData($data,"port_of_destination", ""),"field=port%5Fof%5Fdestination".$keylink,"",MODE_PRINT);
			$record["port_of_destination_value"]=$value;

//	bl_nbr - 
			$value="";
				$value = ProcessLargeText(GetData($data,"bl_nbr", ""),"field=bl%5Fnbr".$keylink,"",MODE_PRINT);
			$record["bl_nbr_value"]=$value;

//	category - 
			$value="";
				$value = ProcessLargeText(GetData($data,"category", ""),"field=category".$keylink,"",MODE_PRINT);
			$record["category_value"]=$value;

//	document_verfied - 
			$value="";
				$value = ProcessLargeText(GetData($data,"document_verfied", ""),"field=document%5Fverfied".$keylink,"",MODE_PRINT);
			$record["document_verfied_value"]=$value;

//	survey_pos_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"survey_pos_id", ""),"field=survey%5Fpos%5Fid".$keylink,"",MODE_PRINT);
			$record["survey_pos_id_value"]=$value;

//	ctr_position - 
			$value="";
				$value = ProcessLargeText(GetData($data,"ctr_position", ""),"field=ctr%5Fposition".$keylink,"",MODE_PRINT);
			$record["ctr_position_value"]=$value;

//	iso_code - 
			$value="";
				$value = ProcessLargeText(GetData($data,"iso_code", ""),"field=iso%5Fcode".$keylink,"",MODE_PRINT);
			$record["iso_code_value"]=$value;

//	eq_size - 
			$value="";
				$value = ProcessLargeText(GetData($data,"eq_size", ""),"field=eq%5Fsize".$keylink,"",MODE_PRINT);
			$record["eq_size_value"]=$value;

//	eq_type - 
			$value="";
				$value = ProcessLargeText(GetData($data,"eq_type", ""),"field=eq%5Ftype".$keylink,"",MODE_PRINT);
			$record["eq_type_value"]=$value;

//	shipping_line_name - 
			$value="";
				$value = ProcessLargeText(GetData($data,"shipping_line_name", ""),"field=shipping%5Fline%5Fname".$keylink,"",MODE_PRINT);
			$record["shipping_line_name_value"]=$value;

//	tare_weight_unit - 
			$value="";
				$value = ProcessLargeText(GetData($data,"tare_weight_unit", ""),"field=tare%5Fweight%5Funit".$keylink,"",MODE_PRINT);
			$record["tare_weight_unit_value"]=$value;

//	payment_release - 
			$value="";
				$value = ProcessLargeText(GetData($data,"payment_release", ""),"field=payment%5Frelease".$keylink,"",MODE_PRINT);
			$record["payment_release_value"]=$value;

//	trx_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"trx_id", ""),"field=trx%5Fid".$keylink,"",MODE_PRINT);
			$record["trx_id_value"]=$value;
			if($col<$colsonpage)
				$record["endrecord_block"]=true;
			$record["grid_recordheader"]=true;
			$record["grid_vrecord"]=true;
			
			if($eventObj->exists("BeforeMoveNextPrint"))
				$eventObj->BeforeMoveNextPrint($data,$row,$record);
				
			$row["grid_record"]["data"][]=$record;
			
			if($eventObj->exists("ListFetchArray"))
				$data = $eventObj->ListFetchArray($rs);
			else
				$data = db_fetch_array($rs);
				
			while($data)
			{
				if($eventObj->exists("BeforeProcessRowPrint"))
				{
					if(!$eventObj->BeforeProcessRowPrint($data))
					{
						if($eventObj->exists("ListFetchArray"))
							$data = $eventObj->ListFetchArray($rs);
						else
							$data = db_fetch_array($rs);
						continue;
					}
				}
				break;
			}
		}
		if($col<=$colsonpage)
		{
			$row["grid_record"]["data"][count($row["grid_record"]["data"])-1]["endrecord_block"]=false;
		}
		$row["grid_rowspace"]=true;
		$row["grid_recordspace"] = array("data"=>array());
		for($i=0;$i<$colsonpage*2-1;$i++)
			$row["grid_recordspace"]["data"][]=true;
		
		$rowinfo["data"][]=$row;
		
		if($all && $records>=30)
		{
			$page=array("grid_row" =>$rowinfo);
			$page["pageno"]=$pageindex;
			$pageindex++;
			$pages[] = $page;
			$records=0;
			$rowinfo=array();
		}
		
	}
	if(count($rowinfo))
	{
		$page=array("grid_row" =>$rowinfo);
		if($all)
			$page["pageno"]=$pageindex;
		$pages[] = $page;
	}
	
	for($i=0;$i<count($pages);$i++)
	{
	 	if($i<count($pages)-1)
			$pages[$i]["begin"]="<div name=page class=printpage>";
		else
		    $pages[$i]["begin"]="<div name=page>";
			
		$pages[$i]["end"]="</div>";
	}

	$page=array();
	$page["data"]=&$pages;
	$xt->assignbyref("page",$page);

//	show totals
//	process totals
	$record=array();
	$total=GetTotals("eq_nbr",$totals["eq_nbr"],"COUNT",$recno-1,"");
	$record["eq_nbr_total"]=$total;
	$record["eq_nbr_showtotal"]=true;
	$xt->assign("totals_record",true);
	if(count($pages))
	{
		$pages[count($pages)-1]["totals_row"]=array("data"=>array(0=>$record));
	}
	
	
//	display master table info
$mastertable=$_SESSION[$strTableName."_mastertable"];
$masterkeys=array();
if($mastertable=="transaction")
{
//	include proper masterprint.php code
	include("include/transaction_masterprint.php");
	$masterkeys[]=@$_SESSION[$strTableName."_masterkey1"];
	$params=array("detailtable"=>"equipment_uses","keys"=>$masterkeys);
	$master=array();
	$master["func"]="DisplayMasterTableInfo_transaction";
	$master["params"]=$params;
	$xt->assignbyref("showmasterfile",$master);
	$xt->assign("mastertable_block",true);
}

$strSQL=$_SESSION[$strTableName."_sql"];

$isPdfView = false;
if (GetTableData($strTableName, ".isUsebuttonHandlers", false) || $isPdfView)
{
	$pageObject->body["begin"] .="<script type=\"text/javascript\" src=\"include/loadfirst.js\"></script>\r\n";
		$pageObject->body["begin"] .= "<script type=\"text/javascript\" src=\"include/lang/".getLangFileName(mlang_getcurrentlang()).".js\"></script>";
	
	$pageObject->fillSetCntrlMaps();
	$pageObject->body['end'] .= '<script>';
	$pageObject->body['end'] .= "window.controlsMap = ".my_json_encode($pageObject->controlsHTMLMap).";";
	$pageObject->body['end'] .= "window.settings = ".my_json_encode($pageObject->jsSettings).";";
	$pageObject->body['end'] .= '</script>';
		$pageObject->body["end"] .= "<script language=\"JavaScript\" src=\"include/runnerJS/RunnerAll.js\"></script>\r\n";
	$pageObject->addCommonJs();
}


if (GetTableData($strTableName, ".isUsebuttonHandlers", false) || $isPdfView)
	$pageObject->body["end"] .= "<script>".$pageObject->PrepareJS()."</script>";

$xt->assignbyref("body",$pageObject->body);
$xt->assign("grid_block",true);

$xt->assign("gkey_fieldheadercolumn",true);
$xt->assign("gkey_fieldheader",true);
$xt->assign("gkey_fieldcolumn",true);
$xt->assign("gkey_fieldfootercolumn",true);
$xt->assign("eq_nbr_fieldheadercolumn",true);
$xt->assign("eq_nbr_fieldheader",true);
$xt->assign("eq_nbr_fieldcolumn",true);
$xt->assign("eq_nbr_fieldfootercolumn",true);
$xt->assign("line_id_fieldheadercolumn",true);
$xt->assign("line_id_fieldheader",true);
$xt->assign("line_id_fieldcolumn",true);
$xt->assign("line_id_fieldfootercolumn",true);
$xt->assign("depo_id_fieldheadercolumn",true);
$xt->assign("depo_id_fieldheader",true);
$xt->assign("depo_id_fieldcolumn",true);
$xt->assign("depo_id_fieldfootercolumn",true);
$xt->assign("shipping_agents_id_fieldheadercolumn",true);
$xt->assign("shipping_agents_id_fieldheader",true);
$xt->assign("shipping_agents_id_fieldcolumn",true);
$xt->assign("shipping_agents_id_fieldfootercolumn",true);
$xt->assign("shipping_agent_name_fieldheadercolumn",true);
$xt->assign("shipping_agent_name_fieldheader",true);
$xt->assign("shipping_agent_name_fieldcolumn",true);
$xt->assign("shipping_agent_name_fieldfootercolumn",true);
$xt->assign("creator_fieldheadercolumn",true);
$xt->assign("creator_fieldheader",true);
$xt->assign("creator_fieldcolumn",true);
$xt->assign("creator_fieldfootercolumn",true);
$xt->assign("created_fieldheadercolumn",true);
$xt->assign("created_fieldheader",true);
$xt->assign("created_fieldcolumn",true);
$xt->assign("created_fieldfootercolumn",true);
$xt->assign("port_of_origin_fieldheadercolumn",true);
$xt->assign("port_of_origin_fieldheader",true);
$xt->assign("port_of_origin_fieldcolumn",true);
$xt->assign("port_of_origin_fieldfootercolumn",true);
$xt->assign("consignee_fieldheadercolumn",true);
$xt->assign("consignee_fieldheader",true);
$xt->assign("consignee_fieldcolumn",true);
$xt->assign("consignee_fieldfootercolumn",true);
$xt->assign("port_of_destination_fieldheadercolumn",true);
$xt->assign("port_of_destination_fieldheader",true);
$xt->assign("port_of_destination_fieldcolumn",true);
$xt->assign("port_of_destination_fieldfootercolumn",true);
$xt->assign("bl_nbr_fieldheadercolumn",true);
$xt->assign("bl_nbr_fieldheader",true);
$xt->assign("bl_nbr_fieldcolumn",true);
$xt->assign("bl_nbr_fieldfootercolumn",true);
$xt->assign("category_fieldheadercolumn",true);
$xt->assign("category_fieldheader",true);
$xt->assign("category_fieldcolumn",true);
$xt->assign("category_fieldfootercolumn",true);
$xt->assign("document_verfied_fieldheadercolumn",true);
$xt->assign("document_verfied_fieldheader",true);
$xt->assign("document_verfied_fieldcolumn",true);
$xt->assign("document_verfied_fieldfootercolumn",true);
$xt->assign("survey_pos_id_fieldheadercolumn",true);
$xt->assign("survey_pos_id_fieldheader",true);
$xt->assign("survey_pos_id_fieldcolumn",true);
$xt->assign("survey_pos_id_fieldfootercolumn",true);
$xt->assign("ctr_position_fieldheadercolumn",true);
$xt->assign("ctr_position_fieldheader",true);
$xt->assign("ctr_position_fieldcolumn",true);
$xt->assign("ctr_position_fieldfootercolumn",true);
$xt->assign("iso_code_fieldheadercolumn",true);
$xt->assign("iso_code_fieldheader",true);
$xt->assign("iso_code_fieldcolumn",true);
$xt->assign("iso_code_fieldfootercolumn",true);
$xt->assign("eq_size_fieldheadercolumn",true);
$xt->assign("eq_size_fieldheader",true);
$xt->assign("eq_size_fieldcolumn",true);
$xt->assign("eq_size_fieldfootercolumn",true);
$xt->assign("eq_type_fieldheadercolumn",true);
$xt->assign("eq_type_fieldheader",true);
$xt->assign("eq_type_fieldcolumn",true);
$xt->assign("eq_type_fieldfootercolumn",true);
$xt->assign("shipping_line_name_fieldheadercolumn",true);
$xt->assign("shipping_line_name_fieldheader",true);
$xt->assign("shipping_line_name_fieldcolumn",true);
$xt->assign("shipping_line_name_fieldfootercolumn",true);
$xt->assign("tare_weight_unit_fieldheadercolumn",true);
$xt->assign("tare_weight_unit_fieldheader",true);
$xt->assign("tare_weight_unit_fieldcolumn",true);
$xt->assign("tare_weight_unit_fieldfootercolumn",true);
$xt->assign("payment_release_fieldheadercolumn",true);
$xt->assign("payment_release_fieldheader",true);
$xt->assign("payment_release_fieldcolumn",true);
$xt->assign("payment_release_fieldfootercolumn",true);
$xt->assign("trx_id_fieldheadercolumn",true);
$xt->assign("trx_id_fieldheader",true);
$xt->assign("trx_id_fieldcolumn",true);
$xt->assign("trx_id_fieldfootercolumn",true);

	$record_header=array("data"=>array());
	$record_footer=array("data"=>array());
	for($i=0;$i<$colsonpage;$i++)
	{
		$rheader=array();
		$rfooter=array();
		if($i<$colsonpage-1)
		{
			$rheader["endrecordheader_block"]=true;
			$rfooter["endrecordheader_block"]=true;
		}
		$record_header["data"][]=$rheader;
		$record_footer["data"][]=$rfooter;
	}
	$xt->assignbyref("record_header",$record_header);
	$xt->assignbyref("record_footer",$record_footer);
	$xt->assign("grid_header",true);
	$xt->assign("grid_footer",true);


$templatefile = "equipment_uses_print.htm";
$pageObject->templatefile = $templatefile;
	
if($eventObj->exists("BeforeShowPrint"))
	$eventObj->BeforeShowPrint($xt,$templatefile);

if(!postvalue("pdf"))
	$xt->display($templatefile);
else
{
	$xt->load_template($templatefile);
	$page = $xt->fetch_loaded();
	$pagewidth=postvalue("width")*1.05;
	$pageheight=postvalue("height")*1.05;
	$landscape=false;
		if($pagewidth>$pageheight)
		{
			$landscape=true;
			if($pagewidth/$pageheight<297/210)
				$pagewidth = 297/210*$pageheight;
		}
		else
		{
			if($pagewidth/$pageheight<210/297)
				$pagewidth = 210/297*$pageheight;
		}
}

?>
