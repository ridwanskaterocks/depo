<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transaction extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Transaction_Model');
        $this->load->model('Depo_Model');
        $this->load->model('Customer_Model');
        $this->load->model('Vessel_Model');
        $this->load->model('Truck_Model');
        $this->load->model('Shipperagent_Model');
        $this->load->model('Shippingline_Model');
        $this->load->model('Isocode_Model');
        $this->load->model('Equipment_Model');
        $this->load->model('Invoice_Model');
        $this->load->library('lib_auth');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        
        $data['page_title'] = APP_NAME . " | All Transaction";

        $find = $this->input->post("trx_id");
        if (!empty($find))
        {
            $data['datas'] = $this->Transaction_Model->get_datae("transaction", $find, "trx_id");
        }
        else
        {
            $data['datas'] = $this->Transaction_Model->get_all_data("transaction");
        }
        $this->load->view('data/all_transaction_v', $data);
    }
    
    public function getcustomer()
    {
        if (!isset($_GET['term']))
            exit;
        $terms = $_GET['term'];
        $data = array();
        $rows = $this->Customer_Model->get_data("customers", 100, 0,$terms);
        foreach ($rows as $row)
        {
            $data[] = array(
                'label' => $row['name'],
                'value' => $row['tax_id']);   // here i am taking name as value so it will display name in text field, you can change it as per your choice.
        }
        echo json_encode($data);
        flush();
    }

    public function add()
    {
        $data['page_title'] = APP_NAME . " | Add New Transaction";

        $this->load->library('form_validation');
        $data['depos'] = $this->Depo_Model->get_all_data("depo");
        $data['trans_type'] = $this->Transaction_Model->get_all_data("transaction_type");
        $data['trx_id'] = $this->Transaction_Model->get_trx_id() + 1;
        $data['truck'] = $this->Truck_Model->get_all_data("trucking_company");

        $this->load->view('data/add_new_transaction_v', $data);
    }

    public function add_detail()
    {
        $trx_id = $this->input->get("trx_id");
        $data['page_title'] = APP_NAME . " | Add New Transaction";

        $this->load->library('form_validation');
        $data['trx_id'] = $trx_id;
        $data['depos'] = $this->Depo_Model->get_all_data("depo");
        $data['truck'] = $this->Truck_Model->get_all_data("trucking_company");
        $data['datas'] = $this->Equipment_Model->get_datas("equipment_uses", $trx_id, "trx_id");

        $this->load->view('data/add_new_transaction_detail_v', $data);
    }
    public function eir(){
        $data['page_title'] = APP_NAME . " | All Transaction";

        $trx_ids = $this->input->post("trx_id");
        if (empty($trx_ids))
        {
            $data['datas'] = array();
        }
        else
        {
            $data['datas'] = $this->Transaction_Model->get_single("transaction", $trx_ids, "trx_id");
        }

        $this->load->view('data/all_transaction_eir_v', $data);
    }
   
    public function save_det()
    {
        $trx=$this->input->post("trx_id");
        $hasil_trx=$this->Transaction_Model->get_single("transaction", $trx, "trx_id");
        
        $input = array(
            "eq_nbr" => $this->input->post("eq_nbr"),
            "line_id" => $this->input->post("line_id"),
            "depo_id" => $hasil_trx['depo_id'],
            "shipping_agents_id" => $this->input->post("shipping_id"),
            "in_time" => date("Y-m-d H:i:s"),
            "out_time" => "0000-00-00 00:00:00",
            "creator" => $_SESSION[SESSION_NAME]['unique_id'],
            "created" => date("Y-m-d H:i:s"),
            "changed" => "0000-00-00 00:00:00",
            "port_of_destination" => $this->input->post("destination"),
            "port_of_origin" => $this->input->post("origin"),
            "consignee" => $hasil_trx['customer_name'],
            "in_truck_license_nbr" => $this->input->post("no_pol"),
            "in_trucking_company_group_id" => $this->input->post("trucking_company_id"),
            "in_trucking_company_name" => "",
            "in_driver_name" => "",
            "depo_name" => $hasil_trx['depo_id'],
            "out_port_of_origin" => "",
            "out_port_of_destination" => "",
            "survey_pos_id" => "CS",
            "ctr_position" => "PREADVISE",
            "iso_code" => $this->input->post("iso_code"),
            "eq_size" => $this->input->post("size"),
            "eq_type" => $this->input->post("type"),
            "shipping_line_name" => "",
            "shipping_line_name" => "",
            "print_eir_complete" => "",
            "ready_to_print_eir_out" => "",
            "print_eir_out_complete" => "",
            "sci_seal_filename" => "",
            "seal_plug_check" => "",
            "ready_to_print_eir" => "",
            "trx_id" => $this->input->post("trx_id"),
            "tare_weight" => $this->input->post("tare_weight"),
            "tare_weight_unit" => "kg",
            "payment_release" => "N",
            "trx_ido" => 0
        );

        $record = $this->Equipment_Model->insert("equipment_uses", $input);
        if ($record)
        {
        }
            redirect(base_url() . "transaction/add_detail?trx_id=" . $this->input->post("trx_ids"));
    }

    public function cancel_detail()
    {
        $trx_id = $this->input->get("trx_id");
        $this->Transaction_Model->delete("transaction", $trx_id, "trx_id");
        $this->Equipment_Model->delete("equipment_uses", $trx_id, "trx_id");

        redirect(base_url() . "transaction/add");
    }

    public function finish_detail()
    {
        redirect(base_url() . "transaction");
    }

    public function cek_code()
    {
        $container_code = $this->input->get("eq_nbr");
        echo "Valid";
    }

    public function view_customer()
    {
        $data['customer'] = $this->Customer_Model->get_all_data("customers");
        $this->load->view('detail/customer_v', $data);
    }

    public function view_iso_code()
    {
        $data['iso'] = $this->Isocode_Model->get_all_data("iso_code");
        $this->load->view('detail/iso_code_v', $data);
    }

    public function view_lines()
    {
        $data['lines'] = $this->Shippingline_Model->get_all_data("shipping_lines");
        $this->load->view('detail/lines_v', $data);
    }

    public function view_viessel()
    {
        $data['vessel'] = $this->Vessel_Model->get_all_data("vessels");
        $this->load->view('detail/vessel_v', $data);
    }

    public function view_shipping()
    {
        $data['shippingagents'] = $this->Shipperagent_Model->get_all_data("shipping_agents");
        $this->load->view('detail/shippingagent_v', $data);
    }
    
    public function view_shipping2()
    {
        $data['shippingagents'] = $this->Shipperagent_Model->get_all_data("shipping_agents");
        $this->load->view('detail/shippingagent2_v', $data);
    }
    
    public function view_origin()
    {
        $data['origin'] = $this->Shipperagent_Model->get_all_data("portcodes");
        $this->load->view('detail/origin_v', $data);
    }
    
    public function view_destination()
    {
        $data['destination'] = $this->Shipperagent_Model->get_all_data("portcodes");
        $this->load->view('detail/destination_v', $data);
    }

    public function save_trans()
    {
        $input = array(
            "trx_id" => $this->input->post("trx_id"),
            "customer_tax_id" => $this->input->post("customer_tax_id"),
            "customer_name" => $this->input->post("customer_name"),
            "vessel_id" => $this->input->post("vessel_id"),
            "vessel_voyage_id" => "",
            "vessel_name" => "",
            "trx_type_id" => $this->input->post("trx_type_id"),
            "trx_type_name" => "",
            "doc_number" => $this->input->post("document_nbr"),
            "created" => date("Y-m-d H:i:s"),
            "creator" => $_SESSION[SESSION_NAME]['unique_id'],
            "changed" => "0000-00-00 00:00:00",
            "changer" => "",
            "start_timestamp" => "0000-00-00 00:00:00",
            "end_timestamp" => "0000-00-00 00:00:00",
            "bat_nbr" => "",
            "shipping_agent_id" => $this->input->post("shipping_id"),
            "shipping_agent_name" => "",
            "trucking_company_id" => $this->input->post("trucking_company_id"),
            "trucking_company_name" => "",
            "depo_release" => "",
            "payment_release" => "N",
            "depo_id" => $this->input->post("depo_id"),
            "truck_license_nbr" => "",
        );

        $record = $this->Transaction_Model->insert("transaction", $input);
        if ($record)
        {
        }
        else
        {
            redirect(base_url() . "transaction/add");
        }
            redirect(base_url() . "transaction/add_detail?trx_id=" . $this->input->post("trx_id"));
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Transaction_Model->get_single("transaction", $id_product, "trx_id");

            if ($product)
            {
                $this->Transaction_Model->delete("transaction", $id_product, "trx_id");

            }
        }
                        redirect(base_url() . "transaction");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Transaction_Model->get_single("transaction", $id_product, "trx_id");
            $out = $this->Transaction_Model->get_single("transaction", $id_product, "trx_id");
            $data['vessel'] = $this->Vessel_Model->get_single("vessels", $out['vessel_id'], "id");
            $data['shippingagent'] = $this->Shipperagent_Model->get_single("shipping_agents", str_replace("_", " ",$out['shipping_agent_id']), "id");
            $data['truck'] = $this->Truck_Model->get_single("trucking_company", $out['trucking_company_id'], "id");
            $data['type'] = $this->Transaction_Model->get_single("transaction_type", $out['trx_type_id'], "id");
            $data['realease'] = $out['payment_release'];
            $data['depo'] = $this->Depo_Model->get_single("depo", $out['depo_id'], "id");

            $data['feet20'] = $this->Transaction_Model->count_type_20($id_product);
            $rate20 = $this->Transaction_Model->get_single("tarrif", 20, "eq_size");
            $data['rate20'] = $rate20['tarrif_rate'];
            $data['amount20'] = $rate20['tarrif_rate'] * $this->Transaction_Model->count_type_20($id_product);

            $data['feet40'] = $this->Transaction_Model->count_type_40($id_product);
            $rate40 = $this->Transaction_Model->get_single("tarrif", 40, "eq_size");
            $data['rate40'] = $rate40['tarrif_rate'];
            $data['amount40'] = $rate40['tarrif_rate'] * $this->Transaction_Model->count_type_40($id_product);

            $data['feet40'] = $this->Transaction_Model->count_type_40($id_product);
            $data['all_box'] = $this->Transaction_Model->count_type_40($id_product) + $this->Transaction_Model->count_type_20($id_product);

            $data['container'] = $this->Equipment_Model->get_datas("equipment_uses", $out['trx_id'], "trx_id");
        }
        else
        {
            redirect(base_url() . "transaction");
        }
        $data['page_title'] = APP_NAME . " | Edit Transaction";

        $this->load->library('form_validation');

        $this->load->view('data/edit_transaction_v', $data);
    }
    
    public function sync_eq(){
        $container_code=$this->input->get("eq_nbr");
        $trx_id=$this->input->get("trx_id");
        $det=$this->Equipment_Model->get_single("equipment_temp",$container_code, "eq_nbr");
        if($det['ready_to_eir_in']=="Y"){
            $oi=$this->Equipment_Model->get_single("equipment_uses", $container_code, "eq_nbr");
            $this->Equipment_Model->update("equipment_uses", $container_code, array("survey_pos_id" => "SERVICEAREA", "ctr_position"=>"READYDELIVERY","creator"=>$_SESSION[SESSION_NAME]['unique_id']), "eq_nbr");
            $this->Equipment_Model->update("equipment_damages", $container_code, array("equse_gkey" =>$oi['gkey']), "eq_nbr");
        }else{
            
        }
        
        if (!empty($trx_id))
        {
            redirect(BASE_URL . "transaction/view/" . $trx_id);
        }
        else
        {
            redirect(BASE_URL . "equipment?eq_nbr=" . $container_code);
        }
        
    }

    public function payment_realease()
    {
        $trx_id = $this->input->get("trx_id");
        $cek = $this->Transaction_Model->get_single("transaction", $trx_id, "trx_id");

        $det=$this->Equipment_Model->get_datas("equipment_uses", $cek['trx_id'], "trx_id");
        foreach ($det as $v)
        {
            $this->Equipment_Model->update("equipment_uses", $trx_id, array("payment_release" => "Y"), "trx_id");
        }
        
        $this->Transaction_Model->update("transaction", $trx_id, array("payment_release" => "Y"), "trx_id");
        
//        $invoice = array(
//            "invoice_nbr" => date("Ymd") . $trx_id,
//            "trx_id" => $trx_id,
//            "qty_of_20" => $this->Transaction_Model->count_type_20($trx_id),
//            "qty_of_40" => $this->Transaction_Model->count_type_40($trx_id),
//            "created" => date("Y-m-d H:i:s"),
//            "creator" => $_SESSION[SESSION_NAME]['unique_id']
//        );
//
//        if ($cek)
//        {
//
//            $this->Invoice_Model->insert("invoice", $invoice);
//        }
        redirect(base_url() . "transaction/view/".$trx_id);
    }
    public function add_invoice(){
        
    }
    public function print_invoice(){
        $data['trx_id'] = $this->input->get("trx_id");
        $cek = $this->Transaction_Model->get_single("transaction", $this->input->get("trx_id"), "trx_id");
        $data['trans'] = $this->Transaction_Model->get_single("transaction", $this->input->get("trx_id"), "trx_id");

        $inv = $this->Transaction_Model->get_single("invoice", $this->input->get("trx_id"), "trx_id");
        
        if (!$inv)
        {
            $invoice = array(
                "invoice_nbr" => date("Ymd") . $this->input->get("trx_id"),
                "trx_id" => $this->input->get("trx_id"),
                "qty_of_20" => $this->Transaction_Model->count_type_20($this->input->get("trx_id")),
                "qty_of_40" => $this->Transaction_Model->count_type_40($this->input->get("trx_id")),
                "created" => date("Y-m-d H:i:s"),
                "created" => (int) $this->Transaction_Model->count_type_20($this->input->get("trx_id")) * 75000 + $this->Transaction_Model->count_type_40($this->input->get("trx_id") * 100000),
                "creator" => $_SESSION[SESSION_NAME]['unique_id']
            );

            $this->Invoice_Model->insert("invoice", $invoice);
        }

        $data['equip'] = $this->Equipment_Model->get_datas("equipment_uses", $this->input->get("trx_id"), "trx_id");
        $this->load->view('detail/invoice', $data);
    }

    public function print_eir()
    {
        $data['eq_nbr'] = $this->input->get("eq_nbr");
        $data['trans'] = $this->Transaction_Model->get_single("equipment_uses", $this->input->get("eq_nbr"), "eq_nbr");
        $data['equip'] = $this->Equipment_Model->get_datas("equipment_damage", $this->input->get("eq_nbr"), "eq_nbr");

        $this->load->view('detail/eir', $data);
    }
    function testpdf()
    {
        $this->load->library('fpdf/fpdf');
        $this->fpdf->FPDF('P', 'cm', 'A4');
        $this->fpdf->AddPage();
        $this->fpdf->SetFont('Arial', '', 10);
        $teks = "Ini hasil Laporan PDF menggunakan Library FPDF di CodeIgniter";
        $this->fpdf->Cell(0, 0, $teks, 0, '0', 'L', true);
        $this->fpdf->Cell(0, 0, "akuikialie", 0, '0', 'L', true);
        $this->fpdf->Ln();
        $this->fpdf->Output();
    }

}