<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_auth');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        
        redirect(base_url()."home/dashboard");
    }

    public function dashboard()
    {
        $this->lib_auth->check('yes');

        $data['page_title'] = "Dashboard";
        $this->load->view("dashboard_v", $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */