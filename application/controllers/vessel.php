<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vessel extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Vessel_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Vessels";

        $data['datas'] = $this->Vessel_Model->get_all_data("vessels");

        $this->load->view('data/all_vessel_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Vessels";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_vessel_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => str_replace(" ", "", ucfirst($this->input->post("id"))),
            "name" => $this->input->post("name"),
            "line_owner" => $this->input->post("line"),
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Vessel_Model->update("vessels", $id, $input, "id");

            redirect(base_url() . "vessel");
        }
        else
        {

            $record = $this->Vessel_Model->insert("vessels", $input);
            if ($record)
            {
                
            }
        }
        redirect(base_url() . "vessel");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Vessel_Model->delete("vessels", $id_product, "id");

            if ($product)
            {
                $this->Vessel_Model->delete("vessels", $id_product, "id");

            }
        }
                redirect(base_url() . "vessel");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Vessel_Model->get_single("vessels", $id_product, "id");
        }
        else
        {
            redirect(base_url() . "vessel");
        }
        $data['page_title'] = APP_NAME . " | Edit Vessels";

        $this->load->library('form_validation');

        $this->load->view('data/edit_vessel_v', $data);
    }

}