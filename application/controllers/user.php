<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('User_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Users";

        $data['datas'] = $this->User_Model->get_all_data("users");

        $this->load->view('data/all_user_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Depo";

        $this->load->library('form_validation');
        $data['depos'] = $this->User_Model->get_all_data("depo");

        $this->load->view('data/add_new_user_v', $data);
    }

    public function save()
    {
        $input = array(
            "unique_id" => $this->input->post("unique_id"),
            "name" => ucfirst($this->input->post("unique_id")),
            "encrypted_password" => $this->input->post("password"),
            "role_id" => $this->input->post("role_id"),
            "depo_id" => $this->input->post("depo_id"),
            "salt" => md5($this->input->post("password"))
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $inputs = array(
                "name" => $this->input->post("name")
            );
            $id = $this->input->post("unique_id");
            $record = $this->User_Model->update("users", $id, $inputs, "uid");

           // $this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "update", "user");
            redirect(base_url() . "user");
        }
        else
        {
            $cek = $this->User_Model->get_single("users", $this->input->post("unique_id"), "unique_id");
            if (!$cek)
            {
                $record = $this->User_Model->insert("users", $input);

                //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "add", "user");
            }
//            echo '<script language="javascript">';
//            echo 'alert("User successfully add")';
//            echo '</script>';
            redirect(base_url() . "user");
        }
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->User_Model->delete("users", str_replace("_", " ", $id_product), "uid");

            if ($product)
            {
                $this->User_Model->delete("users", $id_product, "uid");
                $this->Log_Model->delete("log_users", $id_product, "id_user");
                //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "delete", "user");
            }
        }
        redirect(base_url() . "user");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->User_Model->get_single("users", $id_product, "uid");
        }
        else
        {
            redirect(base_url() . "user");
        }
        $data['page_title'] = APP_NAME . " | Edit Users";

        $this->load->library('form_validation');

        $this->load->view('data/edit_user_v', $data);
    }

}