<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Depo extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Depo_Model", "", TRUE);
    }

    public function get_depo_get()
    {
        $depo = $this->Depo_Model->get_all_data("depo");
        if ($depo)
        {
            $this->response(array("data" => $depo));
        }
        else
        {
            $this->response(array("data" => array()));
        }
    }

}
