<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class User extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("User_Model", "", TRUE);
    }
    
    public function cek_login_get()
    {
        $username = $this->get('unique_id');
        $password = $this->get('encrypted_password');

        $hasil = $this->User_Model->cek_login($username, $password);
        if ($hasil)
        {
            $user = $this->User_Model->get_single("users", $hasil['uid'], "uid");
            
            $this->response(array("status_login" => 1, "data" => $user));
        }
        else
        {
            $this->response(array("status_login" => 0, "error" => ""));
        }
    }
}

/* End of file user.php */
/* Location: ./application/controllers/api/user.php */