<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class Equipment extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Equipment_Model", "", TRUE);
        $this->load->library('lib_auth');
    }
    
    public function cek_digit_get()
    {
        $this->load->library('Functions');
        $out=0;
        $eq_nbr = $this->get('eq_nbr');
        $out = $this->functions->cek_digit($eq_nbr);
        if ($out == 1)
        {
            $out=1;
        }
        else
        {
            $out=0;
        }
        $this->response(array("status" =>$out));
    }
}

/* End of file user.php */
/* Location: ./application/controllers/api/user.php */