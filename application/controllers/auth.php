<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('lib_auth');
        $this->load->model('User_Model');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $data['page'] = "Login";
        $this->load->view("login_v", $data);
    }

    public function login()
    {
        $data['page'] = "Login";
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $out = validation_errors();
        }
        else
        {
            $this->load->model('User_Model');

            $hasil = $this->User_Model->cek_login($email,$password);
            if ($hasil)
            {
                $user_data = $this->User_Model->get_single("users", $email, "unique_id");

                //input log user
                $this->Log_Model->insert_login_users($user_data['uid']);
                $this->lib_auth->save($user_data);
                $out = 1;
            }else{
                $out="Your Account isn't Valid";
            }
        }
        echo $out;
    }

    public function logout()
    {
        //input log user
        $this->Log_Model->insert_logout_users($_SESSION[SESSION_NAME]['uid']);

        $this->lib_auth->logout();
    }

}