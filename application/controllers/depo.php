<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Depo extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Depo_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Depo";

        $data['datas'] = $this->Depo_Model->get_all_data("depo");

        $this->load->view('data/all_depo_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Depo";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_depo_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => str_replace(" ", "", strtoupper($this->input->post("id"))),
            "name" => $this->input->post("name"),
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Depo_Model->update("depo", $id, $input, "id");
            
            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "update", "depo", $input);

            redirect(base_url() . "depo");
        }
        else
        {
            $record = $this->Depo_Model->insert("depo", $input);
            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "add", "depo", $input);
            redirect(base_url() . "depo");
        }
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Depo_Model->delete("depo", $id_product, "id");

            if ($product)
            {
                //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "delete", "depo", $id_product);
                $this->Depo_Model->delete("depo", $id_product, "id");
            }
        }
        redirect(base_url() . "depo");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Depo_Model->get_single("depo", $id_product, "id");
        }
        else
        {
            redirect(base_url() . "depo");
        }
        $data['page_title'] = APP_NAME . " | Edit Depo";

        $this->load->library('form_validation');

        $this->load->view('data/edit_depo_v', $data);
    }

}