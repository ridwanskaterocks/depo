<?php

class Functions {

    function cek_digit($digit)
    {
        $total_a = 0;
        $total_b = 0;
        $array_digit = array();
        $array_digit = str_split($digit);

        $bil = array(
            array("huruf" => "A", "angka" => 10),
            array("huruf" => "B", "angka" => 12),
            array("huruf" => "C", "angka" => 13),
            array("huruf" => "D", "angka" => 14),
            array("huruf" => "E", "angka" => 15),
            array("huruf" => "F", "angka" => 16),
            array("huruf" => "G", "angka" => 17),
            array("huruf" => "H", "angka" => 18),
            array("huruf" => "I", "angka" => 19),
            array("huruf" => "J", "angka" => 20),
            array("huruf" => "K", "angka" => 21),
            array("huruf" => "L", "angka" => 23),
            array("huruf" => "M", "angka" => 24),
            array("huruf" => "N", "angka" => 25),
            array("huruf" => "O", "angka" => 26),
            array("huruf" => "P", "angka" => 27),
            array("huruf" => "Q", "angka" => 28),
            array("huruf" => "R", "angka" => 29),
            array("huruf" => "S", "angka" => 30),
            array("huruf" => "T", "angka" => 31),
            array("huruf" => "U", "angka" => 32),
            array("huruf" => "V", "angka" => 34),
            array("huruf" => "W", "angka" => 35),
            array("huruf" => "X", "angka" => 36),
            array("huruf" => "Y", "angka" => 37),
            array("huruf" => "Z", "angka" => 38)
        );

        $mixed = array();
        for ($a = 0; $a < count($array_digit) - 7; $a++)
        {
            $huruf_bil = "";
            $huruf_abjad = $array_digit[$a]; //A
            for ($b = 0; $b < count($bil); $b++)
            {
                $huruf_bil = $bil[$b]['huruf'];
                if ($huruf_abjad == $huruf_bil)
                {
                    $mixed[] = $bil[$b]['angka'];
                }
            }
        }
        for ($c = 4; $c < 10; $c++)
        {
            $mixed[] = (int) $array_digit[$c];
        }

        $x = 2;
        $oi = 0;
        $kali = array();
        for ($d = 0; $d < 10; $d++)
        {
            $kali[] = exp($d * log((int) $x));
        }

        $hasil = array();
        for ($index = 0; $index < 10; $index++)
        {
            $hasil = $mixed[$index] * $kali[$index];
            $total_a = $total_a + $hasil;
        }
        $total_a_bagi = bcadd($total_a / 11, 0.1, 0);
        $total_b = $total_a_bagi * 11;
        $hasilnya = $total_a - $total_b;
        $digits = bcadd($hasilnya, 0.1, 0);

        $digitnya = $array_digit[10];
        if ($digits == $digitnya)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }

}