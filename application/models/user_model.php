<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_Model extends MY_Model {

    private $table = "users";

    function cek_login($username, $password)
    {
        $query = $this->db->get_where("users", array("unique_id" => $username, "encrypted_password"=>$password));
        $result = $query->row_array();
        return $result;
    }
    
     function cek_user($username)
    {
        $query = $this->db->get_where("users", array("username" => $username));
        $result = $query->row_array();
        return $result;
    }

}